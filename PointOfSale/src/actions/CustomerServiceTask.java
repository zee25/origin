package actions;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import org.jdesktop.swingx.JXTable;

public interface CustomerServiceTask {
    
    
    public double calculatePoints(double total);
    
    public void pointsVariable();
    public ResultSet getRewards();
    public ResultSet getRewardsAvail(String customerPoint);
    public ResultSet getCoupons();
    public ResultSet getPromos();
    public ResultSet getCustomers();
    public ResultSet getNewCustomers();
    public ResultSet getCustomerPoints();
    public ResultSet getPointTransactions(String custID);
    public ResultSet getLastCustId();
    public int getTotalCustomers();
    public ResultSet specificPoint(String custID);
    
    public void addCustomers(String customerId,String lastName, String firstName, String middleName, String gender, String datereg, String contact, String address);
    public void addRewards(String rewardName, String rewardItem, String rewDesc, String equivalentPoints);
    public void addCoupons(String couponCode, String couponDescription, String dateStart, String dateEnd);
    public void addPromos(String promoName,String promoDescription,String promoCategory, String promoDateStart, String promoDateEnd);
    public void addPoints(double pointEarned);
    public void addPointTransactions(String custId, String pointId, String rewardId, String equivalentPoints);
    public void regPoints(String custId);
   
    public void updateRewards(String rewardID, String rewardName, String rewardItem, String rewDesc, String equivalentPoints);
    public void updateCoupons(String couponID, String couponCode, String couponDescription, String dateStart, String dateEnd);
    public void updatePromos(String promoID,String promoName,String promoDescription,String promoCategory, String promoDateStart, String promoDateEnd);
    public void updatePointsVariable(String PointVarVal);
    public void updatePoint(String pointId,String subtPoint);
    public void updatePromos(String promoName,String promoDesc,String promoCategory,String promoDateStart,String promoDateEnd);
    public void updateCustomers(String custID, String lastName, String firstName, String middleName);
    
    
    public ResultSet searchCustomers(String customerName, int SelectedIndex);
    public ResultSet searchRewards(String rewardName, int SelectedIndex);
    public ResultSet searchCoupons(String couponName,int SelectedIndex);
    public ResultSet searchPromos(String promoName, int SelectedIndex);
    
    public void removeCustomer(String customerID);
    public void removeRewards(String rewardID);
    public void removeCoupons(String couponID);
    public void removePromos(String promoID);
    
    public void addLog(String username, String action, String timestamp );
    public void updateLog(String username, String action, String idNum, String timestamp);
    public void removeLog(String username, String action, String idNum, String timestamp);
    
    public ResultSet countCustomers();
    public ResultSet countPromotions();
    public ResultSet countRewards();
    public ResultSet countCoupons();
    
    public String getPointVariable();
    
    public void loadBrand(JComboBox comboBox, String firstIndex, String selCat, String selSub);
    public void loadSubCategory(JComboBox comboBox, String firstIndex, String selCat);
    public void loadCategory(JComboBox comboBox, String firstIndex);
    public void loadProduct(JComboBox comboBox, String firstIndex, String selCat, String selSub, String brand);
  
    public void addCustomer(String customerId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException;
    public void loadCustomer(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException;
    public void updateCustomer(String curCustId, String newCustId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException;
    public void updateCustomerId(String curCustId, String newCustId) throws SQLException;
    public void updateCustomerLastName(String custId, String lastName) throws SQLException;
    public void updateCustomerFirstName(String custId, String firstName) throws SQLException;
    public void updateCustomerMiddleName(String custId, String midName) throws SQLException;
    public void updateCustomerGender(String custId, int gender) throws SQLException;
    public void updateCustomerAddress(String custId, String address) throws SQLException;
    public void updateCustomerContactNumber(String custId, String contactNo) throws SQLException;
}
