package frames;

import connection.DatabaseConnection;
import control.PopUpMessage;
import employee.CustomerService;
import employee.Admin;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;
import javax.swing.JPanel;
import net.proteanit.sql.DbUtils;
import customtable.CustomTableHeader;
import java.awt.Dimension;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;




public class CustomerServiceFrame extends javax.swing.JFrame {
    
    Date timeStamp = new Date();
    PopUpMessage popUp;
    DateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat idFormat = new SimpleDateFormat("yyyyMMdd");
    String coupID, coupCode, coupDesc,desc, dateStart,dateEnd;
    String rewID, rewName, rewItem, rewDesc, equivPoints;
    String promoID, promoName, promoDesc, promoCategory, promoDateStart, promoDateEnd; 
    String custID,genCustID, lastName,firstName,middleName,gender,contact,address,custPoint;
    String dateNow = timeStampFormat.format(timeStamp), nextYear;
    String idDate =idFormat.format(timeStamp);
    String uuid = idDate + UUID.randomUUID().toString().replaceAll("-", "");
    Double pointVar,custPt,subtPt;
    int selectedIndex,rewPrompt;
    
    Calendar cal = Calendar.getInstance();
    Date today = cal.getTime();
    
    static Authentication auth;

    CustomerService customerService = new CustomerService();
    private JPopupMenu accountSetting = new JPopupMenu();
    private Admin admin;
    Calendar now = Calendar.getInstance();
    
    public CustomerServiceFrame(Authentication auth) {
        initComponents();
        this.auth = auth;
        initialize();
        setLocationRelativeTo(null);
        switchTo(dashboard, "Dashboard");
        redeemDialog.setModal(true);
        redeemDialog.setResizable(false);
        redeemDialog.setLocationRelativeTo(null);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        prompt = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        promptIcon = new javax.swing.JLabel();
        promptMsg = new javax.swing.JLabel();
        profileSetting = new javax.swing.JPanel();
        jPanel39 = new javax.swing.JPanel();
        jLabel60 = new javax.swing.JLabel();
        jPanel44 = new javax.swing.JPanel();
        jLabel66 = new javax.swing.JLabel();
        redeemDialog = new javax.swing.JDialog();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jLabel35 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        rewardNameLabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        title = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        main = new javax.swing.JPanel();
        dashboard = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        customerLabel = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        promotionLabel = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        rewardLabel = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        couponLabel = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        pointsTable = new org.jdesktop.swingx.JXTable();
        jScrollPane12 = new javax.swing.JScrollPane();
        newCustomersTable = new org.jdesktop.swingx.JXTable();
        editPointVariable = new javax.swing.JPanel();
        jPanel84 = new javax.swing.JPanel();
        editPointVarButton = new javax.swing.JPanel();
        jLabel147 = new javax.swing.JLabel();
        pointVarValue = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        pointLabel = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        addProductBack7 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        viewProfile = new javax.swing.JPanel();
        jPanel83 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jLabel125 = new javax.swing.JLabel();
        jLabel126 = new javax.swing.JLabel();
        jLabel127 = new javax.swing.JLabel();
        jLabel128 = new javax.swing.JLabel();
        jLabel129 = new javax.swing.JLabel();
        jLabel130 = new javax.swing.JLabel();
        jLabel131 = new javax.swing.JLabel();
        jPanel32 = new javax.swing.JPanel();
        jLabel135 = new javax.swing.JLabel();
        jLabel136 = new javax.swing.JLabel();
        jLabel134 = new javax.swing.JLabel();
        jLabel133 = new javax.swing.JLabel();
        promotions = new javax.swing.JPanel();
        jPanel51 = new javax.swing.JPanel();
        addPromoBtn = new javax.swing.JPanel();
        jLabel92 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        jPanel75 = new javax.swing.JPanel();
        jLabel241 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        searchPromo = new javax.swing.JTextField();
        editPromoBtn = new javax.swing.JLabel();
        deletePromoBtn = new javax.swing.JLabel();
        searchByPromo = new javax.swing.JComboBox<>();
        jLabel54 = new javax.swing.JLabel();
        productTableScroll2 = new javax.swing.JScrollPane();
        promotionsTable = new org.jdesktop.swingx.JXTable();
        coupon = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        addCouponBtn = new javax.swing.JPanel();
        jLabel87 = new javax.swing.JLabel();
        jLabel88 = new javax.swing.JLabel();
        jPanel65 = new javax.swing.JPanel();
        jLabel230 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        searchCoupon = new javax.swing.JTextField();
        searchByCust2 = new javax.swing.JComboBox<>();
        jLabel56 = new javax.swing.JLabel();
        deleteCouponBtn = new javax.swing.JLabel();
        editCouponBtn = new javax.swing.JLabel();
        productTableScroll1 = new javax.swing.JScrollPane();
        couponTable = new org.jdesktop.swingx.JXTable();
        customers = new javax.swing.JPanel();
        jPanel41 = new javax.swing.JPanel();
        addCustomerBtn = new javax.swing.JPanel();
        jLabel229 = new javax.swing.JLabel();
        jLabel231 = new javax.swing.JLabel();
        jPanel67 = new javax.swing.JPanel();
        jLabel232 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        searchCustomer = new javax.swing.JTextField();
        searchByCust = new javax.swing.JComboBox<>();
        jLabel53 = new javax.swing.JLabel();
        custRewardBtn = new javax.swing.JPanel();
        jLabel124 = new javax.swing.JLabel();
        productTableScroll = new javax.swing.JScrollPane();
        customersTable = new org.jdesktop.swingx.JXTable();
        rewards = new javax.swing.JPanel();
        jPanel42 = new javax.swing.JPanel();
        addRewardsBtn = new javax.swing.JPanel();
        jLabel89 = new javax.swing.JLabel();
        jLabel90 = new javax.swing.JLabel();
        jPanel74 = new javax.swing.JPanel();
        jLabel240 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        searchReward = new javax.swing.JTextField();
        editRewardBtn = new javax.swing.JLabel();
        deleteRewardBtn = new javax.swing.JLabel();
        varSettingBtn = new javax.swing.JPanel();
        jLabel95 = new javax.swing.JLabel();
        searchByCust3 = new javax.swing.JComboBox<>();
        jLabel57 = new javax.swing.JLabel();
        productTableScroll3 = new javax.swing.JScrollPane();
        rewardsTable = new org.jdesktop.swingx.JXTable();
        redeem = new javax.swing.JPanel();
        jPanel59 = new javax.swing.JPanel();
        jPanel76 = new javax.swing.JPanel();
        jLabel242 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jTextField5 = new javax.swing.JTextField();
        searchByCust4 = new javax.swing.JComboBox<>();
        jLabel58 = new javax.swing.JLabel();
        jPanel60 = new javax.swing.JPanel();
        jLabel123 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        customerName = new javax.swing.JLabel();
        jLabel108 = new javax.swing.JLabel();
        customerPoints = new javax.swing.JLabel();
        productTableScroll4 = new javax.swing.JScrollPane();
        pointsTransactTable = new org.jdesktop.swingx.JXTable();
        addReward = new javax.swing.JPanel();
        jPanel85 = new javax.swing.JPanel();
        jPanel50 = new javax.swing.JPanel();
        jPanel33 = new javax.swing.JPanel();
        jPanel38 = new javax.swing.JPanel();
        rewardsSaveBtn = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        equivalentPointsText = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        rewardNameText = new javax.swing.JTextField();
        rewardItem = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        rewardDescText = new javax.swing.JTextField();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        addSupplierMessage3 = new javax.swing.JLabel();
        addProductBack6 = new javax.swing.JLabel();
        addCoupon = new javax.swing.JPanel();
        jPanel86 = new javax.swing.JPanel();
        jPanel47 = new javax.swing.JPanel();
        addSupplierMessage2 = new javax.swing.JLabel();
        addProductBack5 = new javax.swing.JLabel();
        jPanel90 = new javax.swing.JPanel();
        couponSaveBtn = new javax.swing.JPanel();
        jLabel63 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        couponDescriptionText = new javax.swing.JTextField();
        jSeparator12 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        couponCodeText = new javax.swing.JTextField();
        couponDateEnd = new org.jdesktop.swingx.JXDatePicker();
        couponDateStart = new org.jdesktop.swingx.JXDatePicker();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        discPercentage = new javax.swing.JTextField();
        jSeparator21 = new javax.swing.JSeparator();
        productIdCbox = new javax.swing.JComboBox<>();
        brandIdCbox = new javax.swing.JComboBox<>();
        categoryIdCbox = new javax.swing.JComboBox<>();
        subCategoryIdCbox = new javax.swing.JComboBox<>();
        addPromo = new javax.swing.JPanel();
        jPanel87 = new javax.swing.JPanel();
        jPanel48 = new javax.swing.JPanel();
        addSupplierMessage1 = new javax.swing.JLabel();
        addProductBack4 = new javax.swing.JLabel();
        jPanel92 = new javax.swing.JPanel();
        jSeparator13 = new javax.swing.JSeparator();
        promoNameText = new javax.swing.JTextField();
        promoSaveBtn = new javax.swing.JPanel();
        jLabel71 = new javax.swing.JLabel();
        promoDescriptionText = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        jLabel72 = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        promoDateEndD8 = new org.jdesktop.swingx.JXDatePicker();
        promoDateStartD8 = new org.jdesktop.swingx.JXDatePicker();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        jLabel79 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        discPercentage1 = new javax.swing.JTextField();
        jSeparator22 = new javax.swing.JSeparator();
        brandIdCbox1 = new javax.swing.JComboBox<>();
        categoryIdCbox1 = new javax.swing.JComboBox<>();
        subCategoryIdCbox1 = new javax.swing.JComboBox<>();
        productIdCbox1 = new javax.swing.JComboBox<>();
        addCustomer = new javax.swing.JPanel();
        jPanel88 = new javax.swing.JPanel();
        jPanel94 = new javax.swing.JPanel();
        addProductBack3 = new javax.swing.JLabel();
        addSupplierMessage = new javax.swing.JLabel();
        jPanel95 = new javax.swing.JPanel();
        jSeparator15 = new javax.swing.JSeparator();
        lastNameText = new javax.swing.JTextField();
        customerSaveBtn = new javax.swing.JPanel();
        jLabel74 = new javax.swing.JLabel();
        genderCBox = new javax.swing.JComboBox<>();
        firstNameText = new javax.swing.JTextField();
        jSeparator16 = new javax.swing.JSeparator();
        addressText = new javax.swing.JTextField();
        jSeparator17 = new javax.swing.JSeparator();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jSeparator19 = new javax.swing.JSeparator();
        jLabel32 = new javax.swing.JLabel();
        jSeparator20 = new javax.swing.JSeparator();
        middleNameText = new javax.swing.JTextField();
        contactText = new javax.swing.JTextField();
        rewardSelect = new javax.swing.JPanel();
        jPanel43 = new javax.swing.JPanel();
        jPanel77 = new javax.swing.JPanel();
        jLabel243 = new javax.swing.JLabel();
        jSeparator18 = new javax.swing.JSeparator();
        searchReward1 = new javax.swing.JTextField();
        addProductBack8 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        searchByCust5 = new javax.swing.JComboBox<>();
        customerPoints1 = new javax.swing.JLabel();
        jLabel109 = new javax.swing.JLabel();
        customerName1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        addRewardsBtn2 = new javax.swing.JPanel();
        jLabel94 = new javax.swing.JLabel();
        jPanel49 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jScrollPane10 = new javax.swing.JScrollPane();
        rewardsTable3 = new org.jdesktop.swingx.JXTable();
        jPanel8 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel30 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        dashBoardBtn = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        customersBtn = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        couponsBtn = new javax.swing.JPanel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        promotionsBtn = new javax.swing.JPanel();
        jLabel263 = new javax.swing.JLabel();
        jLabel264 = new javax.swing.JLabel();
        rewardsBtn = new javax.swing.JPanel();
        jLabel265 = new javax.swing.JLabel();
        jLabel266 = new javax.swing.JLabel();

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));

        promptIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/check.png"))); // NOI18N

        promptMsg.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        promptMsg.setForeground(new java.awt.Color(0, 153, 51));
        promptMsg.setText("PROMPT_MESSAGE");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(promptIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(promptMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 564, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(promptIcon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(promptMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout promptLayout = new javax.swing.GroupLayout(prompt);
        prompt.setLayout(promptLayout);
        promptLayout.setHorizontalGroup(
            promptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        promptLayout.setVerticalGroup(
            promptLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        profileSetting.setLayout(new javax.swing.BoxLayout(profileSetting, javax.swing.BoxLayout.Y_AXIS));

        jPanel39.setBackground(new java.awt.Color(255, 255, 255));
        jPanel39.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel39.setPreferredSize(new java.awt.Dimension(200, 30));

        jLabel60.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel60.setText("Change Password");

        javax.swing.GroupLayout jPanel39Layout = new javax.swing.GroupLayout(jPanel39);
        jPanel39.setLayout(jPanel39Layout);
        jPanel39Layout.setHorizontalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel39Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))
        );
        jPanel39Layout.setVerticalGroup(
            jPanel39Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel60, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        profileSetting.add(jPanel39);

        jPanel44.setBackground(new java.awt.Color(255, 255, 255));
        jPanel44.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel44.setPreferredSize(new java.awt.Dimension(200, 30));
        jPanel44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel44MouseClicked(evt);
            }
        });

        jLabel66.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel66.setText("Log out");

        javax.swing.GroupLayout jPanel44Layout = new javax.swing.GroupLayout(jPanel44);
        jPanel44.setLayout(jPanel44Layout);
        jPanel44Layout.setHorizontalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel44Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel66, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE))
        );
        jPanel44Layout.setVerticalGroup(
            jPanel44Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel66, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
        );

        profileSetting.add(jPanel44);

        redeemDialog.setMinimumSize(new java.awt.Dimension(366, 164));
        redeemDialog.setUndecorated(true);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setMaximumSize(new java.awt.Dimension(366, 164));
        jPanel19.setMinimumSize(new java.awt.Dimension(366, 164));

        jPanel20.setBackground(new java.awt.Color(102, 255, 102));
        jPanel20.setForeground(new java.awt.Color(255, 255, 255));
        jPanel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel20MouseClicked(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel35.setText("CONFIRM");

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel21.setBackground(new java.awt.Color(255, 51, 51));
        jPanel21.setForeground(new java.awt.Color(255, 255, 255));
        jPanel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel21MouseClicked(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.setText("CANCEL");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel37.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel37.setText("Redeeming the Reward");

        rewardNameLabel.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        rewardNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rewardNameLabel.setText("jLabel5");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel37)
                .addGap(130, 130, 130))
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(rewardNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel19Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel37)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rewardNameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout redeemDialogLayout = new javax.swing.GroupLayout(redeemDialog.getContentPane());
        redeemDialog.getContentPane().setLayout(redeemDialogLayout);
        redeemDialogLayout.setHorizontalGroup(
            redeemDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        redeemDialogLayout.setVerticalGroup(
            redeemDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1350, 730));
        setUndecorated(true);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
            }
        });

        jPanel1.setMinimumSize(new java.awt.Dimension(1350, 730));
        jPanel1.setPreferredSize(new java.awt.Dimension(1350, 730));
        jPanel1.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(102, 102, 102));

        jLabel1.setFont(new java.awt.Font("Roboto Medium", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("X");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(1319, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 1360, 20);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(235, 237, 237));

        title.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        title.setForeground(new java.awt.Color(51, 51, 51));
        title.setText("Dashboard");

        jPanel7.setOpaque(false);
        jPanel7.setLayout(null);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/user_profile.png"))); // NOI18N
        jPanel7.add(jLabel4);
        jLabel4.setBounds(10, 0, 50, 60);

        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Customer Service");
        jPanel7.add(jLabel5);
        jLabel5.setBounds(80, 30, 210, 16);

        name.setFont(new java.awt.Font("Roboto Light", 1, 16)); // NOI18N
        name.setForeground(new java.awt.Color(51, 51, 51));
        name.setText("Zobabel Uba");
        jPanel7.add(name);
        name.setBounds(80, 10, 230, 20);

        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dropdown3.png"))); // NOI18N
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel30MouseClicked(evt);
            }
        });
        jPanel7.add(jLabel30);
        jLabel30.setBounds(320, 20, 20, 15);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, 507, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(title, javax.swing.GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        main.setBackground(new java.awt.Color(255, 255, 255));
        main.setLayout(new java.awt.CardLayout());

        dashboard.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(225, 255, 232));

        jLabel7.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        jLabel7.setText("Sales Activity");

        jPanel9.setMinimumSize(new java.awt.Dimension(1108, 182));
        jPanel9.setOpaque(false);
        jPanel9.setPreferredSize(new java.awt.Dimension(1108, 182));
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 22, 5));

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setPreferredSize(new java.awt.Dimension(250, 170));

        customerLabel.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        customerLabel.setForeground(new java.awt.Color(0, 204, 204));
        customerLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        customerLabel.setText("228");

        jLabel10.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Customers");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(customerLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(customerLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel9.add(jPanel10);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setPreferredSize(new java.awt.Dimension(250, 170));

        promotionLabel.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        promotionLabel.setForeground(new java.awt.Color(0, 204, 102));
        promotionLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        promotionLabel.setText("228");

        jLabel13.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Promotions");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(promotionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(promotionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel9.add(jPanel11);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setPreferredSize(new java.awt.Dimension(250, 170));

        rewardLabel.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        rewardLabel.setForeground(new java.awt.Color(255, 204, 0));
        rewardLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rewardLabel.setText("228");

        jLabel16.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Rewards");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(rewardLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rewardLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel9.add(jPanel12);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setPreferredSize(new java.awt.Dimension(250, 170));

        couponLabel.setFont(new java.awt.Font("Roboto Light", 1, 48)); // NOI18N
        couponLabel.setForeground(new java.awt.Color(255, 153, 0));
        couponLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        couponLabel.setText("228");

        jLabel19.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(51, 51, 51));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Coupons");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(couponLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(couponLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jPanel9.add(jPanel13);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 1153, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));

        jLabel38.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel38.setText("Recently Added Customers");

        jLabel62.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel62.setText("Customers with Highest Points");

        pointsTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        pointsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        pointsTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        pointsTable.setRowHeight(30);
        jScrollPane11.setViewportView(pointsTable);

        newCustomersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        newCustomersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        newCustomersTable.setToolTipText("");
        newCustomersTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        newCustomersTable.setRowHeight(30);
        jScrollPane12.setViewportView(newCustomersTable);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(339, 339, 339)))
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jScrollPane11)
                        .addGap(20, 20, 20))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel62)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(jLabel62))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout dashboardLayout = new javax.swing.GroupLayout(dashboard);
        dashboard.setLayout(dashboardLayout);
        dashboardLayout.setHorizontalGroup(
            dashboardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dashboardLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dashboardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        dashboardLayout.setVerticalGroup(
            dashboardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dashboardLayout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 39, Short.MAX_VALUE))
        );

        main.add(dashboard, "card2");

        editPointVariable.setBackground(new java.awt.Color(255, 255, 255));

        jPanel84.setBackground(new java.awt.Color(255, 255, 255));

        editPointVarButton.setBackground(new java.awt.Color(51, 51, 51));
        editPointVarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editPointVarButtonMouseClicked(evt);
            }
        });

        jLabel147.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel147.setForeground(new java.awt.Color(255, 255, 255));
        jLabel147.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel147.setText("Save");

        javax.swing.GroupLayout editPointVarButtonLayout = new javax.swing.GroupLayout(editPointVarButton);
        editPointVarButton.setLayout(editPointVarButtonLayout);
        editPointVarButtonLayout.setHorizontalGroup(
            editPointVarButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editPointVarButtonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel147, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        editPointVarButtonLayout.setVerticalGroup(
            editPointVarButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel147, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
        );

        pointVarValue.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        pointVarValue.setForeground(new java.awt.Color(102, 102, 102));
        pointVarValue.setBorder(null);
        pointVarValue.setMargin(new java.awt.Insets(2, 10, 2, 2));
        pointVarValue.setOpaque(false);

        jSeparator6.setForeground(new java.awt.Color(51, 51, 51));

        jLabel6.setFont(new java.awt.Font("Roboto", 0, 24)); // NOI18N
        jLabel6.setText("Every 100 Peso purchase equals to ");

        pointLabel.setFont(new java.awt.Font("Roboto", 0, 24)); // NOI18N
        pointLabel.setText("1");

        jLabel11.setFont(new java.awt.Font("Roboto", 0, 24)); // NOI18N
        jLabel11.setText("Point(s)");

        addProductBack7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductBack7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        addProductBack7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addProductBack7MouseClicked(evt);
            }
        });

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel12.setText("Default value is 0.01 which equals to 1 Point per 100 Php");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addGap(48, 48, 48))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel12)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel84Layout = new javax.swing.GroupLayout(jPanel84);
        jPanel84.setLayout(jPanel84Layout);
        jPanel84Layout.setHorizontalGroup(
            jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel84Layout.createSequentialGroup()
                .addGap(466, 466, 466)
                .addComponent(pointLabel)
                .addGap(97, 97, 97)
                .addComponent(jLabel11)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel84Layout.createSequentialGroup()
                .addComponent(addProductBack7, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 278, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(351, 351, 351))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel84Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel84Layout.createSequentialGroup()
                        .addGroup(jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pointVarValue, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jSeparator6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)))
                        .addGap(310, 310, 310))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel84Layout.createSequentialGroup()
                        .addComponent(editPointVarButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(454, 454, 454))))
        );
        jPanel84Layout.setVerticalGroup(
            jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel84Layout.createSequentialGroup()
                .addGroup(jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel84Layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(jLabel6))
                    .addComponent(addProductBack7, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(jPanel84Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pointLabel)
                    .addComponent(jLabel11))
                .addGap(108, 108, 108)
                .addComponent(pointVarValue, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(editPointVarButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(173, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout editPointVariableLayout = new javax.swing.GroupLayout(editPointVariable);
        editPointVariable.setLayout(editPointVariableLayout);
        editPointVariableLayout.setHorizontalGroup(
            editPointVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editPointVariableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel84, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        editPointVariableLayout.setVerticalGroup(
            editPointVariableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editPointVariableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel84, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        main.add(editPointVariable, "card2");

        viewProfile.setBackground(new java.awt.Color(255, 255, 255));

        jPanel83.setBackground(new java.awt.Color(255, 255, 255));

        jPanel31.setBackground(new java.awt.Color(255, 255, 255));
        jPanel31.setLayout(null);

        jLabel125.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/profile.png"))); // NOI18N

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel125, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel125, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel31.add(jPanel17);
        jPanel17.setBounds(12, 13, 100, 100);

        jLabel126.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel126.setText("2017-212");
        jPanel31.add(jLabel126);
        jLabel126.setBounds(470, 20, 160, 48);

        jLabel127.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel127.setText("Cashier");
        jPanel31.add(jLabel127);
        jLabel127.setBounds(130, 60, 223, 30);

        jLabel128.setFont(new java.awt.Font("Roboto", 0, 24)); // NOI18N
        jLabel128.setText("Rhianne Lompon");
        jPanel31.add(jLabel128);
        jLabel128.setBounds(130, 20, 223, 48);

        jLabel129.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel129.setText("ID:");
        jPanel31.add(jLabel129);
        jLabel129.setBounds(430, 20, 30, 48);

        jLabel130.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel130.setText("Gender:");
        jPanel31.add(jLabel130);
        jLabel130.setBounds(660, 20, 70, 48);

        jLabel131.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel131.setText("Female");
        jPanel31.add(jLabel131);
        jLabel131.setBounds(740, 20, 130, 48);

        jLabel135.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel135.setText("Password:");

        jLabel136.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel136.setText("pettismylove");

        jLabel134.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jLabel134.setText("Rhianne_25");

        jLabel133.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel133.setText("Username:");

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel32Layout.createSequentialGroup()
                        .addComponent(jLabel135, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel136, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel32Layout.createSequentialGroup()
                        .addComponent(jLabel133)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel134, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel133, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel134))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel135, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel136))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel83Layout = new javax.swing.GroupLayout(jPanel83);
        jPanel83.setLayout(jPanel83Layout);
        jPanel83Layout.setHorizontalGroup(
            jPanel83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel83Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel83Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(673, Short.MAX_VALUE))
                    .addGroup(jPanel83Layout.createSequentialGroup()
                        .addComponent(jPanel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel83Layout.setVerticalGroup(
            jPanel83Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel83Layout.createSequentialGroup()
                .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(471, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout viewProfileLayout = new javax.swing.GroupLayout(viewProfile);
        viewProfile.setLayout(viewProfileLayout);
        viewProfileLayout.setHorizontalGroup(
            viewProfileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(viewProfileLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel83, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        viewProfileLayout.setVerticalGroup(
            viewProfileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, viewProfileLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel83, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        main.add(viewProfile, "card2");

        promotions.setBackground(new java.awt.Color(255, 255, 255));
        promotions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                promotionsMouseClicked(evt);
            }
        });

        jPanel51.setBackground(new java.awt.Color(255, 255, 255));

        addPromoBtn.setBackground(new java.awt.Color(102, 102, 102));
        addPromoBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addPromoBtnMouseClicked(evt);
            }
        });

        jLabel92.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel92.setForeground(new java.awt.Color(255, 255, 255));
        jLabel92.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel92.setText("Add Promo");

        jLabel93.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel93.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N

        javax.swing.GroupLayout addPromoBtnLayout = new javax.swing.GroupLayout(addPromoBtn);
        addPromoBtn.setLayout(addPromoBtnLayout);
        addPromoBtnLayout.setHorizontalGroup(
            addPromoBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addPromoBtnLayout.createSequentialGroup()
                .addComponent(jLabel93, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel92, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addPromoBtnLayout.setVerticalGroup(
            addPromoBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addPromoBtnLayout.createSequentialGroup()
                .addGroup(addPromoBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel93, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(jLabel92, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel75.setBackground(new java.awt.Color(255, 255, 255));
        jPanel75.setLayout(null);

        jLabel241.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel241.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search_icon.png"))); // NOI18N
        jPanel75.add(jLabel241);
        jLabel241.setBounds(0, 0, 45, 30);
        jPanel75.add(jSeparator4);
        jSeparator4.setBounds(50, 30, 250, 10);

        searchPromo.setEditable(false);
        searchPromo.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        searchPromo.setText("Promo Name");
        searchPromo.setBorder(null);
        searchPromo.setOpaque(false);
        searchPromo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchPromoMouseClicked(evt);
            }
        });
        searchPromo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchPromoActionPerformed(evt);
            }
        });
        searchPromo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchPromoKeyTyped(evt);
            }
        });
        jPanel75.add(searchPromo);
        searchPromo.setBounds(50, 2, 250, 30);

        editPromoBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        editPromoBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit.png"))); // NOI18N
        editPromoBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editPromoBtnMouseClicked(evt);
            }
        });

        deletePromoBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        deletePromoBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/delete.png"))); // NOI18N
        deletePromoBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deletePromoBtnMouseClicked(evt);
            }
        });

        searchByPromo.setBackground(new java.awt.Color(204, 255, 255));
        searchByPromo.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchByPromo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Promo Name", "Promo Description" }));
        searchByPromo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                searchByPromoMousePressed(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel54.setText("Search by:");

        javax.swing.GroupLayout jPanel51Layout = new javax.swing.GroupLayout(jPanel51);
        jPanel51.setLayout(jPanel51Layout);
        jPanel51Layout.setHorizontalGroup(
            jPanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel51Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addPromoBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel75, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel54, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(searchByPromo, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 184, Short.MAX_VALUE)
                .addComponent(editPromoBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deletePromoBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel51Layout.setVerticalGroup(
            jPanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel51Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel51Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel75, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addPromoBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editPromoBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deletePromoBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel54, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchByPromo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        productTableScroll2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        productTableScroll2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        customersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        promotionsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        promotionsTable.setAutoStartEditOnKeyStroke(false);
        promotionsTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        promotionsTable.setRowHeight(30);
        promotionsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                promotionsTableMouseReleased(evt);
            }
        });
        productTableScroll2.setViewportView(promotionsTable);

        javax.swing.GroupLayout promotionsLayout = new javax.swing.GroupLayout(promotions);
        promotions.setLayout(promotionsLayout);
        promotionsLayout.setHorizontalGroup(
            promotionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(promotionsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(promotionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(productTableScroll2, javax.swing.GroupLayout.DEFAULT_SIZE, 1110, Short.MAX_VALUE))
                .addContainerGap())
        );
        promotionsLayout.setVerticalGroup(
            promotionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(promotionsLayout.createSequentialGroup()
                .addComponent(jPanel51, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(productTableScroll2, javax.swing.GroupLayout.PREFERRED_SIZE, 644, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        main.add(promotions, "card2");

        coupon.setBackground(new java.awt.Color(255, 255, 255));

        jPanel34.setBackground(new java.awt.Color(255, 255, 255));

        addCouponBtn.setBackground(new java.awt.Color(102, 102, 102));
        addCouponBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addCouponBtnMouseClicked(evt);
            }
        });

        jLabel87.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel87.setForeground(new java.awt.Color(255, 255, 255));
        jLabel87.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel87.setText("Add Coupon");

        jLabel88.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel88.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N

        javax.swing.GroupLayout addCouponBtnLayout = new javax.swing.GroupLayout(addCouponBtn);
        addCouponBtn.setLayout(addCouponBtnLayout);
        addCouponBtnLayout.setHorizontalGroup(
            addCouponBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addCouponBtnLayout.createSequentialGroup()
                .addComponent(jLabel88, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel87, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addCouponBtnLayout.setVerticalGroup(
            addCouponBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addCouponBtnLayout.createSequentialGroup()
                .addGroup(addCouponBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel88, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(jLabel87, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel65.setBackground(new java.awt.Color(255, 255, 255));
        jPanel65.setLayout(null);

        jLabel230.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel230.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search_icon.png"))); // NOI18N
        jPanel65.add(jLabel230);
        jLabel230.setBounds(0, 0, 45, 30);
        jPanel65.add(jSeparator1);
        jSeparator1.setBounds(50, 30, 250, 10);

        searchCoupon.setEditable(false);
        searchCoupon.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        searchCoupon.setText("Coupon Code");
        searchCoupon.setBorder(null);
        searchCoupon.setOpaque(false);
        searchCoupon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchCouponMouseClicked(evt);
            }
        });
        searchCoupon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchCouponActionPerformed(evt);
            }
        });
        searchCoupon.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchCouponKeyTyped(evt);
            }
        });
        jPanel65.add(searchCoupon);
        searchCoupon.setBounds(50, 2, 250, 30);

        searchByCust2.setBackground(new java.awt.Color(204, 255, 255));
        searchByCust2.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchByCust2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Coupon Code", "Coupon Description" }));
        searchByCust2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchByCust2MouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                searchByCust2MouseReleased(evt);
            }
        });

        jLabel56.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel56.setText("Search by:");

        deleteCouponBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        deleteCouponBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/delete.png"))); // NOI18N
        deleteCouponBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteCouponBtnMouseClicked(evt);
            }
        });

        editCouponBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        editCouponBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit.png"))); // NOI18N
        editCouponBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editCouponBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addCouponBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(jPanel65, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(searchByCust2, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 246, Short.MAX_VALUE)
                .addComponent(editCouponBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteCouponBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(editCouponBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteCouponBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel65, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addCouponBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel56, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchByCust2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        productTableScroll1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        productTableScroll1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        customersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        couponTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        couponTable.setAutoStartEditOnKeyStroke(false);
        couponTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        couponTable.setRowHeight(30);
        couponTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                couponTableMouseReleased(evt);
            }
        });
        productTableScroll1.setViewportView(couponTable);

        javax.swing.GroupLayout couponLayout = new javax.swing.GroupLayout(coupon);
        coupon.setLayout(couponLayout);
        couponLayout.setHorizontalGroup(
            couponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(couponLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(productTableScroll1)
                .addContainerGap())
        );
        couponLayout.setVerticalGroup(
            couponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(couponLayout.createSequentialGroup()
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(productTableScroll1)
                .addContainerGap())
        );

        main.add(coupon, "card2");

        customers.setBackground(new java.awt.Color(255, 255, 255));

        jPanel41.setBackground(new java.awt.Color(255, 255, 255));

        addCustomerBtn.setBackground(new java.awt.Color(102, 102, 102));
        addCustomerBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addCustomerBtnMouseClicked(evt);
            }
        });

        jLabel229.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel229.setForeground(new java.awt.Color(255, 255, 255));
        jLabel229.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel229.setText("Add Customer");

        jLabel231.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel231.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N

        javax.swing.GroupLayout addCustomerBtnLayout = new javax.swing.GroupLayout(addCustomerBtn);
        addCustomerBtn.setLayout(addCustomerBtnLayout);
        addCustomerBtnLayout.setHorizontalGroup(
            addCustomerBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addCustomerBtnLayout.createSequentialGroup()
                .addComponent(jLabel231, javax.swing.GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel229, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addCustomerBtnLayout.setVerticalGroup(
            addCustomerBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addCustomerBtnLayout.createSequentialGroup()
                .addGroup(addCustomerBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel231, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(jLabel229, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel67.setBackground(new java.awt.Color(255, 255, 255));
        jPanel67.setLayout(null);

        jLabel232.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel232.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search_icon.png"))); // NOI18N
        jPanel67.add(jLabel232);
        jLabel232.setBounds(0, 0, 45, 30);
        jPanel67.add(jSeparator2);
        jSeparator2.setBounds(50, 30, 250, 10);

        searchCustomer.setEditable(false);
        searchCustomer.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchCustomer.setText("Customer Name");
        searchCustomer.setBorder(null);
        searchCustomer.setOpaque(false);
        searchCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchCustomerMouseClicked(evt);
            }
        });
        searchCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchCustomerActionPerformed(evt);
            }
        });
        searchCustomer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchCustomerKeyTyped(evt);
            }
        });
        jPanel67.add(searchCustomer);
        searchCustomer.setBounds(50, 2, 250, 30);

        searchByCust.setBackground(new java.awt.Color(204, 255, 255));
        searchByCust.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchByCust.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Customer ID", "Last Name", "Middle Name", "First Name" }));
        searchByCust.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchByCustMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                searchByCustMouseReleased(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel53.setText("Search by:");

        custRewardBtn.setBackground(new java.awt.Color(102, 102, 102));
        custRewardBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                custRewardBtnMouseClicked(evt);
            }
        });

        jLabel124.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel124.setForeground(new java.awt.Color(255, 255, 255));
        jLabel124.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel124.setText("Customer Rewards");

        javax.swing.GroupLayout custRewardBtnLayout = new javax.swing.GroupLayout(custRewardBtn);
        custRewardBtn.setLayout(custRewardBtnLayout);
        custRewardBtnLayout.setHorizontalGroup(
            custRewardBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel124, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
        );
        custRewardBtnLayout.setVerticalGroup(
            custRewardBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(custRewardBtnLayout.createSequentialGroup()
                .addComponent(jLabel124, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel41Layout = new javax.swing.GroupLayout(jPanel41);
        jPanel41.setLayout(jPanel41Layout);
        jPanel41Layout.setHorizontalGroup(
            jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel41Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addCustomerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(jPanel67, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel53, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(searchByCust, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(custRewardBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel41Layout.setVerticalGroup(
            jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel41Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel41Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel67, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addCustomerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchByCust)
                    .addComponent(jLabel53, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(custRewardBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        productTableScroll.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        productTableScroll.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        customersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        customersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        customersTable.setAutoStartEditOnKeyStroke(false);
        customersTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        customersTable.setRowHeight(30);
        customersTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                customersTableMouseReleased(evt);
            }
        });
        productTableScroll.setViewportView(customersTable);

        javax.swing.GroupLayout customersLayout = new javax.swing.GroupLayout(customers);
        customers.setLayout(customersLayout);
        customersLayout.setHorizontalGroup(
            customersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customersLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(productTableScroll, javax.swing.GroupLayout.DEFAULT_SIZE, 1153, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        customersLayout.setVerticalGroup(
            customersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customersLayout.createSequentialGroup()
                .addComponent(jPanel41, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(productTableScroll, javax.swing.GroupLayout.PREFERRED_SIZE, 632, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        main.add(customers, "card2");

        rewards.setBackground(new java.awt.Color(255, 255, 255));

        jPanel42.setBackground(new java.awt.Color(255, 255, 255));

        addRewardsBtn.setBackground(new java.awt.Color(102, 102, 102));
        addRewardsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addRewardsBtnMouseClicked(evt);
            }
        });

        jLabel89.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel89.setForeground(new java.awt.Color(255, 255, 255));
        jLabel89.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel89.setText("Add Rewards");

        jLabel90.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel90.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/add.png"))); // NOI18N
        jLabel90.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel90MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout addRewardsBtnLayout = new javax.swing.GroupLayout(addRewardsBtn);
        addRewardsBtn.setLayout(addRewardsBtnLayout);
        addRewardsBtnLayout.setHorizontalGroup(
            addRewardsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addRewardsBtnLayout.createSequentialGroup()
                .addComponent(jLabel90, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel89, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addRewardsBtnLayout.setVerticalGroup(
            addRewardsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel90, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
            .addComponent(jLabel89, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel74.setBackground(new java.awt.Color(255, 255, 255));
        jPanel74.setLayout(null);

        jLabel240.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel240.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search_icon.png"))); // NOI18N
        jPanel74.add(jLabel240);
        jLabel240.setBounds(0, 0, 45, 30);
        jPanel74.add(jSeparator3);
        jSeparator3.setBounds(50, 30, 250, 10);

        searchReward.setEditable(false);
        searchReward.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        searchReward.setText("Reward Name");
        searchReward.setBorder(null);
        searchReward.setOpaque(false);
        searchReward.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchRewardMouseClicked(evt);
            }
        });
        searchReward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchRewardActionPerformed(evt);
            }
        });
        searchReward.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchRewardKeyTyped(evt);
            }
        });
        jPanel74.add(searchReward);
        searchReward.setBounds(50, 2, 250, 30);

        editRewardBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        editRewardBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit.png"))); // NOI18N
        editRewardBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editRewardBtnMouseClicked(evt);
            }
        });

        deleteRewardBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        deleteRewardBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/delete.png"))); // NOI18N
        deleteRewardBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteRewardBtnMouseClicked(evt);
            }
        });

        varSettingBtn.setBackground(new java.awt.Color(102, 102, 102));
        varSettingBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                varSettingBtnMouseClicked(evt);
            }
        });

        jLabel95.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel95.setForeground(new java.awt.Color(255, 255, 255));
        jLabel95.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel95.setText("Point System Setting");

        javax.swing.GroupLayout varSettingBtnLayout = new javax.swing.GroupLayout(varSettingBtn);
        varSettingBtn.setLayout(varSettingBtnLayout);
        varSettingBtnLayout.setHorizontalGroup(
            varSettingBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel95, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
        );
        varSettingBtnLayout.setVerticalGroup(
            varSettingBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel95, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );

        searchByCust3.setBackground(new java.awt.Color(204, 255, 255));
        searchByCust3.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchByCust3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Reward ID", "Reward Name", "Reward Item", "Reward Description", "Equivalent Points" }));
        searchByCust3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchByCust3MouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                searchByCust3MouseReleased(evt);
            }
        });

        jLabel57.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel57.setText("Search by:");

        javax.swing.GroupLayout jPanel42Layout = new javax.swing.GroupLayout(jPanel42);
        jPanel42.setLayout(jPanel42Layout);
        jPanel42Layout.setHorizontalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel42Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addRewardsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(varSettingBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jPanel74, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(searchByCust3, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(editRewardBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deleteRewardBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel42Layout.setVerticalGroup(
            jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel42Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(varSettingBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel74, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addRewardsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel42Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(deleteRewardBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(editRewardBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel57, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchByCust3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        productTableScroll3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        productTableScroll3.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        customersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        rewardsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        rewardsTable.setAutoStartEditOnKeyStroke(false);
        rewardsTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        rewardsTable.setRowHeight(30);
        rewardsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rewardsTableMouseReleased(evt);
            }
        });
        productTableScroll3.setViewportView(rewardsTable);

        javax.swing.GroupLayout rewardsLayout = new javax.swing.GroupLayout(rewards);
        rewards.setLayout(rewardsLayout);
        rewardsLayout.setHorizontalGroup(
            rewardsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel42, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rewardsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(productTableScroll3, javax.swing.GroupLayout.DEFAULT_SIZE, 1117, Short.MAX_VALUE)
                .addContainerGap())
        );
        rewardsLayout.setVerticalGroup(
            rewardsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rewardsLayout.createSequentialGroup()
                .addComponent(jPanel42, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(productTableScroll3, javax.swing.GroupLayout.PREFERRED_SIZE, 644, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        main.add(rewards, "card2");

        redeem.setBackground(new java.awt.Color(255, 255, 255));

        jPanel59.setBackground(new java.awt.Color(255, 255, 255));

        jPanel76.setBackground(new java.awt.Color(255, 255, 255));
        jPanel76.setLayout(null);

        jLabel242.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel242.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search_icon.png"))); // NOI18N
        jPanel76.add(jLabel242);
        jLabel242.setBounds(0, 0, 45, 30);
        jPanel76.add(jSeparator5);
        jSeparator5.setBounds(50, 30, 250, 10);

        jTextField5.setEditable(false);
        jTextField5.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        jTextField5.setBorder(null);
        jTextField5.setOpaque(false);
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });
        jTextField5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField5KeyTyped(evt);
            }
        });
        jPanel76.add(jTextField5);
        jTextField5.setBounds(50, 2, 250, 30);

        searchByCust4.setBackground(new java.awt.Color(204, 255, 255));
        searchByCust4.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchByCust4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchByCust4MouseClicked(evt);
            }
        });

        jLabel58.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel58.setText("Search by:");

        jPanel60.setBackground(new java.awt.Color(102, 102, 102));
        jPanel60.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel60MouseClicked(evt);
            }
        });

        jLabel123.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel123.setForeground(new java.awt.Color(255, 255, 255));
        jLabel123.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel123.setText("Choose Rewards");

        javax.swing.GroupLayout jPanel60Layout = new javax.swing.GroupLayout(jPanel60);
        jPanel60.setLayout(jPanel60Layout);
        jPanel60Layout.setHorizontalGroup(
            jPanel60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel123, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
        );
        jPanel60Layout.setVerticalGroup(
            jPanel60Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel60Layout.createSequentialGroup()
                .addComponent(jLabel123, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel59Layout = new javax.swing.GroupLayout(jPanel59);
        jPanel59.setLayout(jPanel59Layout);
        jPanel59Layout.setHorizontalGroup(
            jPanel59Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel59Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel76, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110)
                .addComponent(jLabel58)
                .addGap(27, 27, 27)
                .addComponent(searchByCust4, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 238, Short.MAX_VALUE)
                .addComponent(jPanel60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );
        jPanel59Layout.setVerticalGroup(
            jPanel59Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel59Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel59Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel76, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel59Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel59Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel58, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(searchByCust4)
                            .addComponent(jPanel60, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel2.setText("Customer Name:");

        customerName.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        customerName.setText("Benmar Eleadion Ayuno");

        jLabel108.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel108.setText("Points:");

        customerPoints.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        customerPoints.setText("100");

        productTableScroll4.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        productTableScroll4.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        customersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(84,84,84)));
        pointsTransactTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        pointsTransactTable.setAutoStartEditOnKeyStroke(false);
        pointsTransactTable.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        pointsTransactTable.setRowHeight(30);
        pointsTransactTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                pointsTransactTableMouseReleased(evt);
            }
        });
        productTableScroll4.setViewportView(pointsTransactTable);

        javax.swing.GroupLayout redeemLayout = new javax.swing.GroupLayout(redeem);
        redeem.setLayout(redeemLayout);
        redeemLayout.setHorizontalGroup(
            redeemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(redeemLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(redeemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel59, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(redeemLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(customerName, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(153, 153, 153)
                        .addComponent(jLabel108)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(customerPoints, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(productTableScroll4, javax.swing.GroupLayout.DEFAULT_SIZE, 1146, Short.MAX_VALUE))
                .addContainerGap())
        );
        redeemLayout.setVerticalGroup(
            redeemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(redeemLayout.createSequentialGroup()
                .addComponent(jPanel59, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(redeemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customerName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel108, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(customerPoints))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(productTableScroll4, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        main.add(redeem, "card2");

        addReward.setBackground(new java.awt.Color(255, 255, 255));
        addReward.setPreferredSize(new java.awt.Dimension(1169, 674));

        jPanel85.setBackground(new java.awt.Color(255, 255, 255));

        jPanel50.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel50Layout = new javax.swing.GroupLayout(jPanel50);
        jPanel50.setLayout(jPanel50Layout);
        jPanel50Layout.setHorizontalGroup(
            jPanel50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 141, Short.MAX_VALUE)
        );
        jPanel50Layout.setVerticalGroup(
            jPanel50Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 532, Short.MAX_VALUE)
        );

        jPanel33.setBackground(new java.awt.Color(255, 255, 255));

        jPanel38.setBackground(new java.awt.Color(255, 255, 255));
        jPanel38.setLayout(null);

        rewardsSaveBtn.setBackground(new java.awt.Color(51, 51, 51));
        rewardsSaveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rewardsSaveBtnMouseClicked(evt);
            }
        });

        jLabel55.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel55.setForeground(new java.awt.Color(255, 255, 255));
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("Save");

        javax.swing.GroupLayout rewardsSaveBtnLayout = new javax.swing.GroupLayout(rewardsSaveBtn);
        rewardsSaveBtn.setLayout(rewardsSaveBtnLayout);
        rewardsSaveBtnLayout.setHorizontalGroup(
            rewardsSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel55, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        rewardsSaveBtnLayout.setVerticalGroup(
            rewardsSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel55, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel38.add(rewardsSaveBtn);
        rewardsSaveBtn.setBounds(100, 310, 160, 40);

        equivalentPointsText.setEditable(false);
        equivalentPointsText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        equivalentPointsText.setForeground(new java.awt.Color(102, 102, 102));
        equivalentPointsText.setBorder(null);
        equivalentPointsText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        equivalentPointsText.setOpaque(false);
        equivalentPointsText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                equivalentPointsTextMouseClicked(evt);
            }
        });
        jPanel38.add(equivalentPointsText);
        equivalentPointsText.setBounds(40, 250, 290, 30);

        jSeparator7.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel38.add(jSeparator7);
        jSeparator7.setBounds(30, 280, 300, 10);

        jSeparator8.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel38.add(jSeparator8);
        jSeparator8.setBounds(30, 140, 300, 10);

        rewardNameText.setEditable(false);
        rewardNameText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        rewardNameText.setForeground(new java.awt.Color(102, 102, 102));
        rewardNameText.setBorder(null);
        rewardNameText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        rewardNameText.setOpaque(false);
        rewardNameText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rewardNameTextMouseClicked(evt);
            }
        });
        rewardNameText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                rewardNameTextKeyTyped(evt);
            }
        });
        jPanel38.add(rewardNameText);
        rewardNameText.setBounds(40, 110, 290, 30);

        rewardItem.setEditable(false);
        rewardItem.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        rewardItem.setForeground(new java.awt.Color(102, 102, 102));
        rewardItem.setBorder(null);
        rewardItem.setMargin(new java.awt.Insets(2, 10, 2, 2));
        rewardItem.setOpaque(false);
        rewardItem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rewardItemMouseClicked(evt);
            }
        });
        rewardItem.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                rewardItemKeyTyped(evt);
            }
        });
        jPanel38.add(rewardItem);
        rewardItem.setBounds(40, 40, 290, 30);

        jSeparator9.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel38.add(jSeparator9);
        jSeparator9.setBounds(30, 70, 300, 10);

        rewardDescText.setEditable(false);
        rewardDescText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        rewardDescText.setForeground(new java.awt.Color(102, 102, 102));
        rewardDescText.setBorder(null);
        rewardDescText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        rewardDescText.setOpaque(false);
        rewardDescText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rewardDescTextMouseClicked(evt);
            }
        });
        rewardDescText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                rewardDescTextKeyTyped(evt);
            }
        });
        jPanel38.add(rewardDescText);
        rewardDescText.setBounds(40, 180, 290, 30);

        jSeparator10.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel38.add(jSeparator10);
        jSeparator10.setBounds(30, 210, 300, 10);

        jLabel14.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel14.setText("Equivalent Points");
        jPanel38.add(jLabel14);
        jLabel14.setBounds(30, 220, 140, 27);

        jLabel15.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel15.setText("Reward Item");
        jPanel38.add(jLabel15);
        jLabel15.setBounds(30, 10, 140, 27);

        jLabel17.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel17.setText("Reward Name");
        jPanel38.add(jLabel17);
        jLabel17.setBounds(30, 80, 140, 27);

        jLabel18.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel18.setText("Reward Description");
        jPanel38.add(jLabel18);
        jLabel18.setBounds(30, 150, 140, 27);

        addSupplierMessage3.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        addSupplierMessage3.setForeground(new java.awt.Color(255, 102, 102));
        addSupplierMessage3.setToolTipText("fghfhfg");
        addSupplierMessage3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addSupplierMessage3MouseClicked(evt);
            }
        });

        addProductBack6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductBack6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        addProductBack6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addProductBack6MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel33Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, 364, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(358, 358, 358))
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addProductBack6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(addSupplierMessage3, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(283, Short.MAX_VALUE))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addProductBack6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSupplierMessage3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                .addComponent(jPanel38, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        javax.swing.GroupLayout jPanel85Layout = new javax.swing.GroupLayout(jPanel85);
        jPanel85.setLayout(jPanel85Layout);
        jPanel85Layout.setHorizontalGroup(
            jPanel85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel33, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel85Layout.createSequentialGroup()
                    .addGap(32, 32, 32)
                    .addComponent(jPanel50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(972, Short.MAX_VALUE)))
        );
        jPanel85Layout.setVerticalGroup(
            jPanel85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel85Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel85Layout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(jPanel50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(22, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout addRewardLayout = new javax.swing.GroupLayout(addReward);
        addReward.setLayout(addRewardLayout);
        addRewardLayout.setHorizontalGroup(
            addRewardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addRewardLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel85, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        addRewardLayout.setVerticalGroup(
            addRewardLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addRewardLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel85, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        main.add(addReward, "card2");

        addCoupon.setBackground(new java.awt.Color(255, 255, 255));

        jPanel86.setBackground(new java.awt.Color(255, 255, 255));

        jPanel47.setBackground(new java.awt.Color(255, 255, 255));

        addSupplierMessage2.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        addSupplierMessage2.setForeground(new java.awt.Color(255, 102, 102));
        addSupplierMessage2.setToolTipText("fghfhfg");
        addSupplierMessage2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addSupplierMessage2MouseClicked(evt);
            }
        });

        addProductBack5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductBack5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        addProductBack5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addProductBack5MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel47Layout = new javax.swing.GroupLayout(jPanel47);
        jPanel47.setLayout(jPanel47Layout);
        jPanel47Layout.setHorizontalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel47Layout.createSequentialGroup()
                .addComponent(addProductBack5, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(addSupplierMessage2, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel47Layout.setVerticalGroup(
            jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel47Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel47Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addProductBack5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSupplierMessage2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel90.setBackground(new java.awt.Color(255, 255, 255));
        jPanel90.setLayout(null);

        couponSaveBtn.setBackground(new java.awt.Color(51, 51, 51));
        couponSaveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                couponSaveBtnMouseClicked(evt);
            }
        });

        jLabel63.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel63.setForeground(new java.awt.Color(255, 255, 255));
        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel63.setText("Save");

        javax.swing.GroupLayout couponSaveBtnLayout = new javax.swing.GroupLayout(couponSaveBtn);
        couponSaveBtn.setLayout(couponSaveBtnLayout);
        couponSaveBtnLayout.setHorizontalGroup(
            couponSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel63, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        couponSaveBtnLayout.setVerticalGroup(
            couponSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel63, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel90.add(couponSaveBtn);
        couponSaveBtn.setBounds(560, 460, 160, 40);

        jLabel64.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel64.setText("Date Started:");
        jPanel90.add(jLabel64);
        jLabel64.setBounds(20, 180, 120, 30);

        jLabel65.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel65.setText("Date End:");
        jPanel90.add(jLabel65);
        jLabel65.setBounds(20, 250, 120, 30);

        couponDescriptionText.setEditable(false);
        couponDescriptionText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        couponDescriptionText.setForeground(new java.awt.Color(102, 102, 102));
        couponDescriptionText.setBorder(null);
        couponDescriptionText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        couponDescriptionText.setOpaque(false);
        couponDescriptionText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                couponDescriptionTextMouseClicked(evt);
            }
        });
        jPanel90.add(couponDescriptionText);
        couponDescriptionText.setBounds(30, 130, 290, 30);

        jSeparator12.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel90.add(jSeparator12);
        jSeparator12.setBounds(20, 160, 300, 10);
        jPanel90.add(jSeparator11);
        jSeparator11.setBounds(820, 200, 350, 10);

        couponCodeText.setEditable(false);
        couponCodeText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        couponCodeText.setForeground(new java.awt.Color(102, 102, 102));
        couponCodeText.setBorder(null);
        couponCodeText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        couponCodeText.setOpaque(false);
        couponCodeText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                couponCodeTextMouseClicked(evt);
            }
        });
        couponCodeText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                couponCodeTextActionPerformed(evt);
            }
        });
        jPanel90.add(couponCodeText);
        couponCodeText.setBounds(30, 50, 290, 30);
        jPanel90.add(couponDateEnd);
        couponDateEnd.setBounds(30, 280, 290, 40);
        jPanel90.add(couponDateStart);
        couponDateStart.setBounds(30, 210, 290, 40);

        jLabel23.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel23.setText("Coupon Description");
        jPanel90.add(jLabel23);
        jLabel23.setBounds(20, 100, 160, 27);

        jLabel24.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel24.setText("Coupon Code");
        jPanel90.add(jLabel24);
        jLabel24.setBounds(20, 20, 160, 27);

        jLabel67.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel67.setForeground(new java.awt.Color(255, 255, 255));
        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel67.setText("Save");
        jPanel90.add(jLabel67);
        jLabel67.setBounds(390, -150, 30, 19);

        jLabel68.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel68.setText("Brand ID:");
        jPanel90.add(jLabel68);
        jLabel68.setBounds(410, 30, 120, 30);

        jLabel69.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel69.setText("Category ID:");
        jPanel90.add(jLabel69);
        jLabel69.setBounds(410, 100, 120, 30);

        jLabel70.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel70.setForeground(new java.awt.Color(255, 255, 255));
        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel70.setText("Save");
        jPanel90.add(jLabel70);
        jLabel70.setBounds(390, 0, 30, 19);

        jLabel75.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel75.setText("Sub Category ID:");
        jPanel90.add(jLabel75);
        jLabel75.setBounds(410, 180, 120, 30);

        jLabel76.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel76.setText("Product ID:");
        jPanel90.add(jLabel76);
        jLabel76.setBounds(410, 250, 120, 30);

        jLabel33.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel33.setText("Discount Percentage");
        jPanel90.add(jLabel33);
        jLabel33.setBounds(810, 110, 160, 27);

        discPercentage.setEditable(false);
        discPercentage.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        discPercentage.setForeground(new java.awt.Color(102, 102, 102));
        discPercentage.setBorder(null);
        discPercentage.setMargin(new java.awt.Insets(2, 10, 2, 2));
        discPercentage.setOpaque(false);
        discPercentage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                discPercentageMouseClicked(evt);
            }
        });
        discPercentage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discPercentageActionPerformed(evt);
            }
        });
        jPanel90.add(discPercentage);
        discPercentage.setBounds(820, 140, 350, 60);
        jPanel90.add(jSeparator21);
        jSeparator21.setBounds(20, 80, 300, 10);

        productIdCbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel90.add(productIdCbox);
        productIdCbox.setBounds(410, 280, 290, 40);

        brandIdCbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel90.add(brandIdCbox);
        brandIdCbox.setBounds(410, 60, 290, 40);

        categoryIdCbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel90.add(categoryIdCbox);
        categoryIdCbox.setBounds(410, 140, 290, 40);

        subCategoryIdCbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel90.add(subCategoryIdCbox);
        subCategoryIdCbox.setBounds(410, 210, 290, 40);

        javax.swing.GroupLayout jPanel86Layout = new javax.swing.GroupLayout(jPanel86);
        jPanel86.setLayout(jPanel86Layout);
        jPanel86Layout.setHorizontalGroup(
            jPanel86Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel47, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel86Layout.createSequentialGroup()
                .addComponent(jPanel90, javax.swing.GroupLayout.PREFERRED_SIZE, 1167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel86Layout.setVerticalGroup(
            jPanel86Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel86Layout.createSequentialGroup()
                .addComponent(jPanel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jPanel90, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 36, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout addCouponLayout = new javax.swing.GroupLayout(addCoupon);
        addCoupon.setLayout(addCouponLayout);
        addCouponLayout.setHorizontalGroup(
            addCouponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addCouponLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel86, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        addCouponLayout.setVerticalGroup(
            addCouponLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel86, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        main.add(addCoupon, "card2");

        addPromo.setBackground(new java.awt.Color(255, 255, 255));

        jPanel87.setBackground(new java.awt.Color(255, 255, 255));

        jPanel48.setBackground(new java.awt.Color(255, 255, 255));

        addSupplierMessage1.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        addSupplierMessage1.setForeground(new java.awt.Color(255, 102, 102));
        addSupplierMessage1.setToolTipText("fghfhfg");
        addSupplierMessage1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addSupplierMessage1MouseClicked(evt);
            }
        });

        addProductBack4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductBack4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        addProductBack4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addProductBack4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel48Layout = new javax.swing.GroupLayout(jPanel48);
        jPanel48.setLayout(jPanel48Layout);
        jPanel48Layout.setHorizontalGroup(
            jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel48Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addProductBack4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(addSupplierMessage1, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(270, Short.MAX_VALUE))
        );
        jPanel48Layout.setVerticalGroup(
            jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel48Layout.createSequentialGroup()
                .addGroup(jPanel48Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addProductBack4, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSupplierMessage1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 78, Short.MAX_VALUE))
        );

        jPanel92.setBackground(new java.awt.Color(255, 255, 255));
        jPanel92.setLayout(null);
        jPanel92.add(jSeparator13);
        jSeparator13.setBounds(30, 130, 310, 10);

        promoNameText.setEditable(false);
        promoNameText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        promoNameText.setForeground(new java.awt.Color(102, 102, 102));
        promoNameText.setBorder(null);
        promoNameText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        promoNameText.setOpaque(false);
        promoNameText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                promoNameTextMouseClicked(evt);
            }
        });
        jPanel92.add(promoNameText);
        promoNameText.setBounds(30, 100, 310, 30);

        promoSaveBtn.setBackground(new java.awt.Color(51, 51, 51));
        promoSaveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                promoSaveBtnMouseClicked(evt);
            }
        });

        jLabel71.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel71.setForeground(new java.awt.Color(255, 255, 255));
        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel71.setText("Save");

        javax.swing.GroupLayout promoSaveBtnLayout = new javax.swing.GroupLayout(promoSaveBtn);
        promoSaveBtn.setLayout(promoSaveBtnLayout);
        promoSaveBtnLayout.setHorizontalGroup(
            promoSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel71, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        promoSaveBtnLayout.setVerticalGroup(
            promoSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel71, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel92.add(promoSaveBtn);
        promoSaveBtn.setBounds(550, 540, 160, 40);

        promoDescriptionText.setEditable(false);
        promoDescriptionText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        promoDescriptionText.setForeground(new java.awt.Color(102, 102, 102));
        promoDescriptionText.setBorder(null);
        promoDescriptionText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        promoDescriptionText.setOpaque(false);
        promoDescriptionText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                promoDescriptionTextMouseClicked(evt);
            }
        });
        jPanel92.add(promoDescriptionText);
        promoDescriptionText.setBounds(30, 170, 310, 30);

        jSeparator14.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel92.add(jSeparator14);
        jSeparator14.setBounds(30, 200, 310, 10);

        jLabel72.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel72.setText("Date Started:");
        jPanel92.add(jLabel72);
        jLabel72.setBounds(10, 220, 120, 30);

        jLabel73.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel73.setText("Date End:");
        jPanel92.add(jLabel73);
        jLabel73.setBounds(20, 310, 120, 30);
        jPanel92.add(promoDateEndD8);
        promoDateEndD8.setBounds(30, 340, 310, 40);
        jPanel92.add(promoDateStartD8);
        promoDateStartD8.setBounds(30, 260, 310, 40);

        jLabel25.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel25.setText("Promotion Name");
        jPanel92.add(jLabel25);
        jLabel25.setBounds(10, 70, 160, 27);

        jLabel26.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel26.setText("Promotion Description");
        jPanel92.add(jLabel26);
        jLabel26.setBounds(10, 140, 160, 27);

        jLabel77.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel77.setText("Brand ID:");
        jPanel92.add(jLabel77);
        jLabel77.setBounds(410, 90, 120, 30);

        jLabel78.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel78.setText("Category ID:");
        jPanel92.add(jLabel78);
        jLabel78.setBounds(410, 160, 120, 30);

        jLabel79.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel79.setText("Sub Category ID:");
        jPanel92.add(jLabel79);
        jLabel79.setBounds(410, 240, 120, 30);

        jLabel80.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel80.setText("Product ID:");
        jPanel92.add(jLabel80);
        jLabel80.setBounds(410, 310, 120, 30);

        jLabel39.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel39.setText("Discount Percentage");
        jPanel92.add(jLabel39);
        jLabel39.setBounds(810, 170, 160, 27);

        discPercentage1.setEditable(false);
        discPercentage1.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        discPercentage1.setForeground(new java.awt.Color(102, 102, 102));
        discPercentage1.setBorder(null);
        discPercentage1.setMargin(new java.awt.Insets(2, 10, 2, 2));
        discPercentage1.setOpaque(false);
        discPercentage1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                discPercentage1MouseClicked(evt);
            }
        });
        discPercentage1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discPercentage1ActionPerformed(evt);
            }
        });
        jPanel92.add(discPercentage1);
        discPercentage1.setBounds(820, 200, 350, 60);
        jPanel92.add(jSeparator22);
        jSeparator22.setBounds(820, 260, 350, 10);

        brandIdCbox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel92.add(brandIdCbox1);
        brandIdCbox1.setBounds(410, 120, 290, 40);

        categoryIdCbox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel92.add(categoryIdCbox1);
        categoryIdCbox1.setBounds(410, 200, 290, 40);

        subCategoryIdCbox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel92.add(subCategoryIdCbox1);
        subCategoryIdCbox1.setBounds(410, 270, 290, 40);

        productIdCbox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "" }));
        jPanel92.add(productIdCbox1);
        productIdCbox1.setBounds(410, 340, 290, 40);

        javax.swing.GroupLayout jPanel87Layout = new javax.swing.GroupLayout(jPanel87);
        jPanel87.setLayout(jPanel87Layout);
        jPanel87Layout.setHorizontalGroup(
            jPanel87Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel48, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel87Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel92, javax.swing.GroupLayout.PREFERRED_SIZE, 1039, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel87Layout.setVerticalGroup(
            jPanel87Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel87Layout.createSequentialGroup()
                .addComponent(jPanel48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel92, javax.swing.GroupLayout.DEFAULT_SIZE, 619, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout addPromoLayout = new javax.swing.GroupLayout(addPromo);
        addPromo.setLayout(addPromoLayout);
        addPromoLayout.setHorizontalGroup(
            addPromoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel87, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        addPromoLayout.setVerticalGroup(
            addPromoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addPromoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel87, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        main.add(addPromo, "card2");

        addCustomer.setBackground(new java.awt.Color(255, 255, 255));

        jPanel88.setBackground(new java.awt.Color(255, 255, 255));

        jPanel94.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel94Layout = new javax.swing.GroupLayout(jPanel94);
        jPanel94.setLayout(jPanel94Layout);
        jPanel94Layout.setHorizontalGroup(
            jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 54, Short.MAX_VALUE)
        );
        jPanel94Layout.setVerticalGroup(
            jPanel94Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );

        addProductBack3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductBack3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        addProductBack3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addProductBack3MouseClicked(evt);
            }
        });

        addSupplierMessage.setFont(new java.awt.Font("Roboto Light", 1, 18)); // NOI18N
        addSupplierMessage.setForeground(new java.awt.Color(255, 102, 102));
        addSupplierMessage.setToolTipText("fghfhfg");
        addSupplierMessage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addSupplierMessageMouseClicked(evt);
            }
        });

        jPanel95.setBackground(new java.awt.Color(255, 255, 255));
        jPanel95.setLayout(null);
        jPanel95.add(jSeparator15);
        jSeparator15.setBounds(20, 70, 300, 10);

        lastNameText.setEditable(false);
        lastNameText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        lastNameText.setForeground(new java.awt.Color(102, 102, 102));
        lastNameText.setBorder(null);
        lastNameText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        lastNameText.setOpaque(false);
        lastNameText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lastNameTextMouseClicked(evt);
            }
        });
        jPanel95.add(lastNameText);
        lastNameText.setBounds(20, 40, 290, 30);

        customerSaveBtn.setBackground(new java.awt.Color(51, 51, 51));
        customerSaveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                customerSaveBtnMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                customerSaveBtnMouseEntered(evt);
            }
        });

        jLabel74.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel74.setForeground(new java.awt.Color(255, 255, 255));
        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel74.setText("Save");

        javax.swing.GroupLayout customerSaveBtnLayout = new javax.swing.GroupLayout(customerSaveBtn);
        customerSaveBtn.setLayout(customerSaveBtnLayout);
        customerSaveBtnLayout.setHorizontalGroup(
            customerSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel74, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
        );
        customerSaveBtnLayout.setVerticalGroup(
            customerSaveBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel74, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel95.add(customerSaveBtn);
        customerSaveBtn.setBounds(90, 460, 160, 40);

        genderCBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gender", "Female", "Male" }));
        jPanel95.add(genderCBox);
        genderCBox.setBounds(20, 380, 300, 40);

        firstNameText.setEditable(false);
        firstNameText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        firstNameText.setForeground(new java.awt.Color(102, 102, 102));
        firstNameText.setBorder(null);
        firstNameText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        firstNameText.setOpaque(false);
        firstNameText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                firstNameTextMouseClicked(evt);
            }
        });
        jPanel95.add(firstNameText);
        firstNameText.setBounds(20, 110, 290, 30);

        jSeparator16.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel95.add(jSeparator16);
        jSeparator16.setBounds(20, 140, 300, 10);

        addressText.setEditable(false);
        addressText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        addressText.setForeground(new java.awt.Color(102, 102, 102));
        addressText.setBorder(null);
        addressText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        addressText.setOpaque(false);
        addressText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addressTextMouseClicked(evt);
            }
        });
        jPanel95.add(addressText);
        addressText.setBounds(20, 320, 290, 30);

        jSeparator17.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel95.add(jSeparator17);
        jSeparator17.setBounds(20, 210, 300, 10);

        jLabel27.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel27.setText("Middle Name");
        jPanel95.add(jLabel27);
        jLabel27.setBounds(10, 150, 160, 27);

        jLabel28.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel28.setText("Last Name");
        jPanel95.add(jLabel28);
        jLabel28.setBounds(10, 0, 160, 27);

        jLabel29.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel29.setText("First Name");
        jPanel95.add(jLabel29);
        jLabel29.setBounds(10, 80, 160, 27);

        jLabel31.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel31.setText("Contact Number");
        jPanel95.add(jLabel31);
        jLabel31.setBounds(10, 220, 160, 27);

        jSeparator19.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel95.add(jSeparator19);
        jSeparator19.setBounds(20, 280, 300, 10);

        jLabel32.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jLabel32.setText("Address");
        jPanel95.add(jLabel32);
        jLabel32.setBounds(10, 290, 160, 27);

        jSeparator20.setPreferredSize(new java.awt.Dimension(50, 10));
        jPanel95.add(jSeparator20);
        jSeparator20.setBounds(20, 350, 300, 10);

        middleNameText.setEditable(false);
        middleNameText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        middleNameText.setForeground(new java.awt.Color(102, 102, 102));
        middleNameText.setBorder(null);
        middleNameText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        middleNameText.setOpaque(false);
        middleNameText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                middleNameTextMouseClicked(evt);
            }
        });
        jPanel95.add(middleNameText);
        middleNameText.setBounds(20, 180, 290, 30);

        contactText.setEditable(false);
        contactText.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        contactText.setForeground(new java.awt.Color(102, 102, 102));
        contactText.setBorder(null);
        contactText.setMargin(new java.awt.Insets(2, 10, 2, 2));
        contactText.setOpaque(false);
        contactText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                contactTextMouseClicked(evt);
            }
        });
        jPanel95.add(contactText);
        contactText.setBounds(20, 250, 290, 30);

        javax.swing.GroupLayout jPanel88Layout = new javax.swing.GroupLayout(jPanel88);
        jPanel88.setLayout(jPanel88Layout);
        jPanel88Layout.setHorizontalGroup(
            jPanel88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel88Layout.createSequentialGroup()
                .addGap(347, 347, 347)
                .addComponent(jPanel94, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel95, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(406, 406, 406))
            .addGroup(jPanel88Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addProductBack3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(addSupplierMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel88Layout.setVerticalGroup(
            jPanel88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel88Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addProductBack3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSupplierMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel88Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel88Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jPanel94, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel88Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jPanel95, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout addCustomerLayout = new javax.swing.GroupLayout(addCustomer);
        addCustomer.setLayout(addCustomerLayout);
        addCustomerLayout.setHorizontalGroup(
            addCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel88, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        addCustomerLayout.setVerticalGroup(
            addCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel88, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        main.add(addCustomer, "card2");

        rewardSelect.setBackground(new java.awt.Color(255, 255, 255));

        jPanel43.setBackground(new java.awt.Color(255, 255, 255));

        jPanel77.setBackground(new java.awt.Color(255, 255, 255));
        jPanel77.setLayout(null);

        jLabel243.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel243.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/search_icon.png"))); // NOI18N
        jPanel77.add(jLabel243);
        jLabel243.setBounds(0, 0, 45, 30);
        jPanel77.add(jSeparator18);
        jSeparator18.setBounds(50, 30, 250, 10);

        searchReward1.setEditable(false);
        searchReward1.setFont(new java.awt.Font("Roboto Light", 0, 18)); // NOI18N
        searchReward1.setText("Reward Name");
        searchReward1.setBorder(null);
        searchReward1.setOpaque(false);
        searchReward1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchReward1MouseClicked(evt);
            }
        });
        searchReward1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchReward1ActionPerformed(evt);
            }
        });
        searchReward1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchReward1KeyTyped(evt);
            }
        });
        jPanel77.add(searchReward1);
        searchReward1.setBounds(50, 2, 250, 30);

        addProductBack8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addProductBack8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/back1.png"))); // NOI18N
        addProductBack8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addProductBack8MouseClicked(evt);
            }
        });

        jLabel59.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel59.setText("Search by:");

        searchByCust5.setBackground(new java.awt.Color(204, 255, 255));
        searchByCust5.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        searchByCust5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Reward ID", "Reward Name", "Reward Item", "Reward Description", "Equivalent Points" }));
        searchByCust5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchByCust5MouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                searchByCust5MouseReleased(evt);
            }
        });

        customerPoints1.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        customerPoints1.setText("100");

        jLabel109.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel109.setText("Available Points:");

        customerName1.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        customerName1.setText("Benmar Eleadion Ayuno");

        jLabel8.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel8.setText("Customer Name:");

        addRewardsBtn2.setBackground(new java.awt.Color(102, 102, 102));
        addRewardsBtn2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addRewardsBtn2MouseClicked(evt);
            }
        });

        jLabel94.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel94.setForeground(new java.awt.Color(255, 255, 255));
        jLabel94.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel94.setText("Redeem");

        javax.swing.GroupLayout addRewardsBtn2Layout = new javax.swing.GroupLayout(addRewardsBtn2);
        addRewardsBtn2.setLayout(addRewardsBtn2Layout);
        addRewardsBtn2Layout.setHorizontalGroup(
            addRewardsBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel94, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
        );
        addRewardsBtn2Layout.setVerticalGroup(
            addRewardsBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel94, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel43Layout = new javax.swing.GroupLayout(jPanel43);
        jPanel43.setLayout(jPanel43Layout);
        jPanel43Layout.setHorizontalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(customerName1)
                .addGap(140, 140, 140)
                .addComponent(jLabel109)
                .addGap(27, 27, 27)
                .addComponent(customerPoints1)
                .addContainerGap(380, Short.MAX_VALUE))
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(addProductBack8, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel77, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(searchByCust5, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(addRewardsBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel43Layout.setVerticalGroup(
            jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel43Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel43Layout.createSequentialGroup()
                        .addComponent(addProductBack8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(customerName1)
                            .addComponent(jLabel109)
                            .addComponent(customerPoints1)))
                    .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(addRewardsBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel43Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel77, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel59, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(searchByCust5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel49.setBackground(new java.awt.Color(255, 255, 255));

        jLabel34.setFont(new java.awt.Font("Roboto", 0, 18)); // NOI18N
        jLabel34.setText("Redeemable Rewards");

        jScrollPane10.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jScrollPane10MouseClicked(evt);
            }
        });

        rewardsTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        rewardsTable3.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        rewardsTable3.setShowHorizontalLines(false);
        rewardsTable3.setShowVerticalLines(false);
        rewardsTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                rewardsTable3MouseReleased(evt);
            }
        });
        jScrollPane10.setViewportView(rewardsTable3);

        javax.swing.GroupLayout jPanel49Layout = new javax.swing.GroupLayout(jPanel49);
        jPanel49.setLayout(jPanel49Layout);
        jPanel49Layout.setHorizontalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel34)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel49Layout.createSequentialGroup()
                    .addContainerGap(13, Short.MAX_VALUE)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 1131, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(13, Short.MAX_VALUE)))
        );
        jPanel49Layout.setVerticalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel34)
                .addContainerGap(529, Short.MAX_VALUE))
            .addGroup(jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel49Layout.createSequentialGroup()
                    .addContainerGap(50, Short.MAX_VALUE)
                    .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 478, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(38, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout rewardSelectLayout = new javax.swing.GroupLayout(rewardSelect);
        rewardSelect.setLayout(rewardSelectLayout);
        rewardSelectLayout.setHorizontalGroup(
            rewardSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rewardSelectLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(rewardSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel43, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel49, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        rewardSelectLayout.setVerticalGroup(
            rewardSelectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rewardSelectLayout.createSequentialGroup()
                .addComponent(jPanel43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        main.add(rewardSelect, "card2");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(main, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(main, javax.swing.GroupLayout.DEFAULT_SIZE, 703, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4);
        jPanel4.setBounds(230, 20, 1130, 740);

        jPanel8.setBackground(new java.awt.Color(84, 84, 84));

        jLabel3.setFont(new java.awt.Font("Roboto Light", 1, 20)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Customer Service");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel8);
        jPanel8.setBounds(0, 20, 230, 60);

        jPanel30.setBackground(new java.awt.Color(102, 102, 102));

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.Y_AXIS));

        dashBoardBtn.setBackground(new java.awt.Color(84, 84, 84));
        dashBoardBtn.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                dashBoardBtnComponentAdded(evt);
            }
        });
        dashBoardBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dashBoardBtnMouseClicked(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Dashboard");

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/dashboard.png"))); // NOI18N

        javax.swing.GroupLayout dashBoardBtnLayout = new javax.swing.GroupLayout(dashBoardBtn);
        dashBoardBtn.setLayout(dashBoardBtnLayout);
        dashBoardBtnLayout.setHorizontalGroup(
            dashBoardBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dashBoardBtnLayout.createSequentialGroup()
                .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        dashBoardBtnLayout.setVerticalGroup(
            dashBoardBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        jPanel3.add(dashBoardBtn);

        customersBtn.setBackground(new java.awt.Color(84, 84, 84));
        customersBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                customersBtnMouseClicked(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Customers");

        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/customer_icon.png"))); // NOI18N

        javax.swing.GroupLayout customersBtnLayout = new javax.swing.GroupLayout(customersBtn);
        customersBtn.setLayout(customersBtnLayout);
        customersBtnLayout.setHorizontalGroup(
            customersBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, customersBtnLayout.createSequentialGroup()
                .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        customersBtnLayout.setVerticalGroup(
            customersBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        jPanel3.add(customersBtn);

        couponsBtn.setBackground(new java.awt.Color(84, 84, 84));
        couponsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                couponsBtnMouseClicked(evt);
            }
        });

        jLabel51.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 255, 255));
        jLabel51.setText("Coupons");

        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/coupon.png"))); // NOI18N

        javax.swing.GroupLayout couponsBtnLayout = new javax.swing.GroupLayout(couponsBtn);
        couponsBtn.setLayout(couponsBtnLayout);
        couponsBtnLayout.setHorizontalGroup(
            couponsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, couponsBtnLayout.createSequentialGroup()
                .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        couponsBtnLayout.setVerticalGroup(
            couponsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel52, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        jPanel3.add(couponsBtn);

        promotionsBtn.setBackground(new java.awt.Color(84, 84, 84));
        promotionsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                promotionsBtnMouseClicked(evt);
            }
        });

        jLabel263.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel263.setForeground(new java.awt.Color(255, 255, 255));
        jLabel263.setText("Promotions");

        jLabel264.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel264.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/sales-icon.png"))); // NOI18N

        javax.swing.GroupLayout promotionsBtnLayout = new javax.swing.GroupLayout(promotionsBtn);
        promotionsBtn.setLayout(promotionsBtnLayout);
        promotionsBtnLayout.setHorizontalGroup(
            promotionsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, promotionsBtnLayout.createSequentialGroup()
                .addComponent(jLabel264, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel263, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        promotionsBtnLayout.setVerticalGroup(
            promotionsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel263, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel264, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        jPanel3.add(promotionsBtn);

        rewardsBtn.setBackground(new java.awt.Color(84, 84, 84));
        rewardsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rewardsBtnMouseClicked(evt);
            }
        });

        jLabel265.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jLabel265.setForeground(new java.awt.Color(255, 255, 255));
        jLabel265.setText("Rewards");

        jLabel266.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel266.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/rewards.png"))); // NOI18N

        javax.swing.GroupLayout rewardsBtnLayout = new javax.swing.GroupLayout(rewardsBtn);
        rewardsBtn.setLayout(rewardsBtnLayout);
        rewardsBtnLayout.setHorizontalGroup(
            rewardsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rewardsBtnLayout.createSequentialGroup()
                .addComponent(jLabel266, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel265, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        rewardsBtnLayout.setVerticalGroup(
            rewardsBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel265, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel266, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        jPanel3.add(rewardsBtn);

        javax.swing.GroupLayout jPanel30Layout = new javax.swing.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel30Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel30Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(417, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel30);
        jPanel30.setBounds(0, 80, 230, 680);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1360, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        this.dispose();
    }//GEN-LAST:event_jLabel1MouseClicked

    private void searchCouponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchCouponActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchCouponActionPerformed

    private void searchCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchCustomerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchCustomerActionPerformed

    private void searchRewardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchRewardActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchRewardActionPerformed

    private void searchPromoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchPromoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchPromoActionPerformed

    private void dashBoardBtnComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_dashBoardBtnComponentAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_dashBoardBtnComponentAdded

    private void dashBoardBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dashBoardBtnMouseClicked
        switchTo(dashboard, "Dashboard");
        loadNewCustomers();
        loadCustomerPoints();
    }//GEN-LAST:event_dashBoardBtnMouseClicked

    private void customersBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customersBtnMouseClicked
        switchTo(customers, "Customers");
       
    }//GEN-LAST:event_customersBtnMouseClicked

    private void couponsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_couponsBtnMouseClicked
        switchTo(coupon, "Coupons");
       
    }//GEN-LAST:event_couponsBtnMouseClicked

    private void promotionsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promotionsBtnMouseClicked
        switchTo(promotions, "Promotions");
       
    }//GEN-LAST:event_promotionsBtnMouseClicked

    private void rewardsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardsBtnMouseClicked
        switchTo(rewards,"Rewards");
        
    }//GEN-LAST:event_rewardsBtnMouseClicked

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void custRewardBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_custRewardBtnMouseClicked
        switchTo(redeem,"Reward Transactions");
        loadPointTransactions();
    }//GEN-LAST:event_custRewardBtnMouseClicked

    private void promotionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promotionsMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_promotionsMouseClicked

    private void addPromoBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addPromoBtnMouseClicked
        switchTo(addPromo,"Add Promotions");
    }//GEN-LAST:event_addPromoBtnMouseClicked

    private void addCouponBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addCouponBtnMouseClicked
        switchTo(addCoupon,"Add Coupons");
    }//GEN-LAST:event_addCouponBtnMouseClicked

    private void addCustomerBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addCustomerBtnMouseClicked
        switchTo(addCustomer,"Add Customers");
    }//GEN-LAST:event_addCustomerBtnMouseClicked

    private void editPointVarButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editPointVarButtonMouseClicked
        customerService.updatePointsVariable(pointVarValue.getText());
        calculatePoint();
        String getVar = pointVarValue.getText();
        updatePointVar(getVar);
    }//GEN-LAST:event_editPointVarButtonMouseClicked

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        initialize();
    }//GEN-LAST:event_formWindowGainedFocus

    private void rewardsSaveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardsSaveBtnMouseClicked
        
        rewItem = rewardItem.getText();
        rewName = rewardNameText.getText();
        rewDesc = rewardDescText.getText();
        equivPoints = equivalentPointsText.getText();
        
        if(title.getText().equals("Add Rewards")){
            if(!rewItem.isEmpty() && !rewName.isEmpty() && !rewDesc.isEmpty() && !equivPoints.isEmpty()){
                customerService.addRewards(rewName,rewItem, rewDesc, equivPoints);
                switchTo(rewards,"Rewards");
                popUp.showMessage("Succesfully added Rewards!", 0);
            }
            else{
                popUp.showMessage("Empty Fields!", 1);
            }
        }
        else{
            if(!rewItem.isEmpty() && !rewName.isEmpty() && !rewDesc.isEmpty() && !equivPoints.isEmpty()){
                customerService.updateRewards(rewID,rewName, rewItem, rewDesc, equivPoints);
                switchTo(rewards,"Rewards");
                popUp.showMessage("Succesfully Updated Rewards!", 0);
              
              
            }
            else{
                popUp.showMessage("Empty Fields!", 1);
            }
        }
        switchTo(rewards,"Rewards");
        loadRewards();
        customerService.addLog(name.getText(), title.getText(), dateNow);
    }//GEN-LAST:event_rewardsSaveBtnMouseClicked

    private void couponSaveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_couponSaveBtnMouseClicked
        
        coupCode = couponCodeText.getText();
        desc = couponDescriptionText.getText();
        dateStart = formatter.format(couponDateStart.getDate());
        dateEnd = formatter.format(couponDateEnd.getDate());
        
        
        if(title.getText().equals("Add Coupons")){
            if(!coupCode.isEmpty() && !desc.isEmpty() && !dateStart.isEmpty() && !dateEnd.isEmpty()){
                customerService.addCoupons(coupCode,desc,dateStart,dateEnd);
                switchTo(coupon,"Coupons");
                popUp.showMessage("Succesfully Added Coupon!", 0);
                
            }
            else{
               // addSupplierMessage.setText("Some fields are empty!");
            }
        }
        else{
            if(!coupCode.isEmpty() && !desc.isEmpty() && !dateStart.isEmpty() && !dateEnd.isEmpty()){
                customerService.updateCoupons(coupID,coupCode,desc,dateStart,dateEnd);
                switchTo(coupon,"Coupons");
                popUp.showMessage("Succesfully Updated Coupon!", 0);
              
            }
            else{
                //addSupplierMessage.setText("Some fields are empty!");
            }
        }
        loadCoupons();
        customerService.addLog(name.getText(), title.getText(), dateNow);// TODO add your handling code here:
    }//GEN-LAST:event_couponSaveBtnMouseClicked

    private void searchRewardKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchRewardKeyTyped
        selectedIndex = searchByCust3.getSelectedIndex();      
        searchRewards();        // TODO add your handling code here:
    }//GEN-LAST:event_searchRewardKeyTyped

    private void searchCouponKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchCouponKeyTyped
        selectedIndex = searchByCust2.getSelectedIndex();
        searchCoupons();    
    }//GEN-LAST:event_searchCouponKeyTyped

    private void editCouponBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editCouponBtnMouseClicked
        switchTo(addCoupon,"Edit Coupon");
        couponCodeText.setText(coupCode);
        couponDescriptionText.setText(coupDesc);    
    }//GEN-LAST:event_editCouponBtnMouseClicked

    private void deleteCouponBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteCouponBtnMouseClicked
        int selectedRow = couponTable.getSelectedRow();
        coupID = (couponTable.getValueAt(selectedRow, 0).toString());
        removeCoupons();
        customerService.removeLog(name.getText(), title.getText(), coupID, dateNow);
    }//GEN-LAST:event_deleteCouponBtnMouseClicked

    private void editRewardBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editRewardBtnMouseClicked
        switchTo(addReward,"Edit Reward");
        rewardItem.setText(rewItem);
        rewardNameText.setText(rewName);
        rewardDescText.setText(rewDesc);
        equivalentPointsText.setText(equivPoints);
    }//GEN-LAST:event_editRewardBtnMouseClicked

    private void deleteRewardBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteRewardBtnMouseClicked
        int selectedRow = rewardsTable.getSelectedRow();
        rewID = (rewardsTable.getValueAt(selectedRow, 0).toString());
        removeRewards(); 
        customerService.removeLog(name.getText(), title.getText(), rewID, dateNow);// TODO add your handling code here:
    }//GEN-LAST:event_deleteRewardBtnMouseClicked

    private void searchPromoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchPromoMouseClicked
        searchPromo.setEditable(true);
        searchPromo.setText("");         // TODO add your handling code here:
    }//GEN-LAST:event_searchPromoMouseClicked

    private void searchPromoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchPromoKeyTyped
        selectedIndex = searchByPromo.getSelectedIndex();
        searchPromos();        // TODO add your handling code here:
    }//GEN-LAST:event_searchPromoKeyTyped

    private void editPromoBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editPromoBtnMouseClicked
        switchTo(addPromo,"Edit Promotion");
        promoNameText.setText(promoName);
        promoDescriptionText.setText(promoDesc);
                // TODO add your handling code here:
    }//GEN-LAST:event_editPromoBtnMouseClicked

    private void deletePromoBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deletePromoBtnMouseClicked
        int selectedRow = promotionsTable.getSelectedRow();
        promoID = (promotionsTable.getValueAt(selectedRow, 0).toString());
        removePromos();
        loadPromos();
        customerService.removeLog(name.getText(), title.getText(), promoID, dateNow);        // TODO add your handling code here:
    }//GEN-LAST:event_deletePromoBtnMouseClicked

    private void searchCouponMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchCouponMouseClicked
        searchCoupon.setEditable(true);
        searchCoupon.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_searchCouponMouseClicked

    private void searchRewardMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchRewardMouseClicked
        searchReward.setEditable(true);
        searchReward.setText("");     // TODO add your handling code here:
    }//GEN-LAST:event_searchRewardMouseClicked

    private void promoSaveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promoSaveBtnMouseClicked
       
                
        promoName= promoNameText.getText();
        promoDesc = promoDescriptionText.getText();
        promoCategory = String.valueOf(categoryIdCbox1.getSelectedIndex());
        promoDateStart = formatter.format(promoDateStartD8.getDate()); 
        promoDateEnd = formatter.format(promoDateEndD8.getDate());
                
        if(title.getText().equals("Add Promotions")){
            if(!promoName.isEmpty() && !promoDesc.isEmpty() && !promoCategory.isEmpty()){
                customerService.addPromos(promoName,promoDesc, promoCategory,promoDateStart,promoDateEnd);
                switchTo(promotions,"Promotions");
                popUp.showMessage("Succesfully Added Promo!", 0);
            }
            else{
               // addSupplierMessage.setText("Some fields are empty!");
            }
        }
        else{
            if(!rewItem.isEmpty() && !rewName.isEmpty() && !equivPoints.isEmpty()){
                customerService.updatePromos(promoID,promoName,promoDesc, promoCategory,promoDateStart,promoDateEnd);
                switchTo(promotions,"Promotions");
                popUp.showMessage("Succesfully Updated Promo!", 0);
              
            }
            else{
                //addSupplierMessage.setText("Some fields are empty!");
            }
        } 
        loadPromos();        
        customerService.addLog(name.getText(), title.getText(), dateNow);// TODO add your handling code here:
    }//GEN-LAST:event_promoSaveBtnMouseClicked

    private void searchCustomerKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchCustomerKeyTyped
        selectedIndex = searchByCust.getSelectedIndex();
        searchCustomers();        // TODO add your handling code here:
    }//GEN-LAST:event_searchCustomerKeyTyped

    private void rewardNameTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardNameTextMouseClicked
        rewardNameText.setEditable(true);
        rewardNameText.setText("");
    }//GEN-LAST:event_rewardNameTextMouseClicked

    private void equivalentPointsTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_equivalentPointsTextMouseClicked
        equivalentPointsText.setEditable(true);
        equivalentPointsText.setText("");// TODO add your handling code here:
    }//GEN-LAST:event_equivalentPointsTextMouseClicked

    private void rewardNameTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rewardNameTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_rewardNameTextKeyTyped

    private void couponCodeTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_couponCodeTextMouseClicked
        couponCodeText.setEditable(true);
        couponCodeText.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_couponCodeTextMouseClicked

    private void couponDescriptionTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_couponDescriptionTextMouseClicked
        couponDescriptionText.setEditable(true);
        couponDescriptionText.setText("");// TODO add your handling code here:
    }//GEN-LAST:event_couponDescriptionTextMouseClicked

    private void promoNameTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promoNameTextMouseClicked
        promoNameText.setEditable(true);
        promoNameText.setText("");// TODO add your handling code here:
    }//GEN-LAST:event_promoNameTextMouseClicked

    private void promoDescriptionTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promoDescriptionTextMouseClicked
        promoDescriptionText.setEditable(true);
        promoDescriptionText.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_promoDescriptionTextMouseClicked

    private void promoDateEndD8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promoDateEndD8MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_promoDateEndD8MouseClicked

    private void searchCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchCustomerMouseClicked
        searchCustomer.setEditable(true);
        searchCustomer.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_searchCustomerMouseClicked

    private void addSupplierMessage1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addSupplierMessage1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_addSupplierMessage1MouseClicked

    private void addProductBack4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addProductBack4MouseClicked
        switchTo(promotions,"Promotions");        // TODO add your handling code here:
    }//GEN-LAST:event_addProductBack4MouseClicked

    private void addSupplierMessage2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addSupplierMessage2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_addSupplierMessage2MouseClicked

    private void addProductBack5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addProductBack5MouseClicked
        switchTo(coupon,"Coupons");        // TODO add your handling code here:
    }//GEN-LAST:event_addProductBack5MouseClicked

    private void addSupplierMessage3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addSupplierMessage3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_addSupplierMessage3MouseClicked

    private void addProductBack6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addProductBack6MouseClicked
        switchTo(rewards,"Rewards");        // TODO add your handling code here:
    }//GEN-LAST:event_addProductBack6MouseClicked

    private void addSupplierMessageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addSupplierMessageMouseClicked

    }//GEN-LAST:event_addSupplierMessageMouseClicked

    private void addProductBack3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addProductBack3MouseClicked
        switchTo(customers,"Customers");
    }//GEN-LAST:event_addProductBack3MouseClicked

    private void addressTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addressTextMouseClicked
        addressText.setEditable(true);
        addressText.setText("");    // TODO add your handling code here:
    }//GEN-LAST:event_addressTextMouseClicked

    private void firstNameTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_firstNameTextMouseClicked
        firstNameText.setEditable(true);
        firstNameText.setText("");    // TODO add your handling code here:
    }//GEN-LAST:event_firstNameTextMouseClicked

    private void customerSaveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customerSaveBtnMouseClicked
        lastName = lastNameText.getText();
        firstName= firstNameText.getText();
        middleName = middleNameText.getText();
        gender = String.valueOf(genderCBox.getSelectedIndex());       
        contact = contactText.getText();
        address = addressText.getText();
            
        if(title.getText().equals("Add Customers")){
            if(!lastName.isEmpty() && !firstName.isEmpty() && !middleName.isEmpty() && !gender.isEmpty()){
                int year = now.get(Calendar.YEAR);
                genCustID = year+"-"+String.valueOf(customerService.getTotalCustomers()+ 1);
                customerService.addCustomers(genCustID,lastName,firstName, middleName, gender, dateNow,contact, address);
                customerService.regPoints(genCustID);
                switchTo(customers,"Customers");
                popUp.showMessage("Succesfully Added Customer!", 0);
            }
            else{
                 addSupplierMessage.setText("Some fields are empty!");
            }
        }
        else{
            if(!lastName.isEmpty() && !firstName.isEmpty() && !middleName.isEmpty() && !gender.isEmpty()){
                customerService.updateCustomers(custID,lastName,firstName, middleName);
                switchTo(customers,"Customers");
                popUp.showMessage("Succesfully Updated Customer!", 0);

            }
            else{
                addSupplierMessage.setText("Some fields are empty!");
            }
        }
        loadCustomers(); 
        customerService.addLog(name.getText(), title.getText(), dateNow);
        popUp.showMessage("Customer added Successfully", 0);
    }//GEN-LAST:event_customerSaveBtnMouseClicked

    private void lastNameTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lastNameTextMouseClicked
        lastNameText.setEditable(true);
        lastNameText.setText("");    // TODO add your handling code here:
    }//GEN-LAST:event_lastNameTextMouseClicked

    private void rewardItemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardItemMouseClicked
        rewardItem.setEditable(true);
        rewardItem.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_rewardItemMouseClicked

    private void rewardItemKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rewardItemKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_rewardItemKeyTyped

    private void customerSaveBtnMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customerSaveBtnMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_customerSaveBtnMouseEntered

    private void rewardDescTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardDescTextMouseClicked
        rewardDescText.setEditable(true);
        rewardDescText.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_rewardDescTextMouseClicked

    private void rewardDescTextKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rewardDescTextKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_rewardDescTextKeyTyped

    private void searchReward1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchReward1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_searchReward1MouseClicked

    private void searchReward1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchReward1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchReward1ActionPerformed

    private void searchReward1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchReward1KeyTyped
         selectedIndex = searchByCust5.getSelectedIndex();
         searchRewards();// TODO add your handling code here:
    }//GEN-LAST:event_searchReward1KeyTyped

    private void addRewardsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addRewardsBtnMouseClicked
        switchTo(addReward, "Add Rewards");
    }//GEN-LAST:event_addRewardsBtnMouseClicked

    private void jLabel90MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel90MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel90MouseClicked

    private void varSettingBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_varSettingBtnMouseClicked
        switchTo(editPointVariable,"Point System Setting");      
    }//GEN-LAST:event_varSettingBtnMouseClicked

    private void addProductBack7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addProductBack7MouseClicked
        switchTo(dashboard,"Dashboard");        // TODO add your handling code here:
    }//GEN-LAST:event_addProductBack7MouseClicked

    private void addProductBack8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addProductBack8MouseClicked
        switchTo(redeem,"Reward Transactions");       // TODO add your handling code here:
    }//GEN-LAST:event_addProductBack8MouseClicked

    private void searchByCustMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCustMouseReleased
        
    }//GEN-LAST:event_searchByCustMouseReleased

    private void searchByCustMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCustMouseClicked
        
    }//GEN-LAST:event_searchByCustMouseClicked

    private void searchByCust2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust2MouseClicked
                // TODO add your handling code here:
    }//GEN-LAST:event_searchByCust2MouseClicked

    private void searchByCust2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust2MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_searchByCust2MouseReleased

    private void searchByCust3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust3MouseClicked
        
    }//GEN-LAST:event_searchByCust3MouseClicked

    private void searchByCust3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust3MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_searchByCust3MouseReleased

    private void searchByCust5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust5MouseClicked
               // TODO add your handling code here:
    }//GEN-LAST:event_searchByCust5MouseClicked

    private void searchByCust5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust5MouseReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_searchByCust5MouseReleased

    private void searchByCust4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByCust4MouseClicked
               // TODO add your handling code here:
    }//GEN-LAST:event_searchByCust4MouseClicked

    private void searchByPromoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchByPromoMousePressed
               // TODO add your handling code here:
    }//GEN-LAST:event_searchByPromoMousePressed

    private void jTextField5KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField5KeyTyped
        selectedIndex = searchByCust4.getSelectedIndex(); 
        searchRewards();        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5KeyTyped

    private void jPanel60MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel60MouseClicked
        
        rewardsTable3.setModel(DbUtils.resultSetToTableModel(customerService.getRewardsAvail(custPoint)));
        switchTo(rewardSelect,"Select Rewards");
    }//GEN-LAST:event_jPanel60MouseClicked

    private void jLabel30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseClicked
        accountSetting.add(profileSetting);
        accountSetting.show(jLabel30, -180, 25);
    }//GEN-LAST:event_jLabel30MouseClicked

    private void jPanel44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel44MouseClicked
        setVisible(false);
        auth.setVisible(true);
    }//GEN-LAST:event_jPanel44MouseClicked

    private void middleNameTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_middleNameTextMouseClicked
        middleNameText.setEditable(true);        // TODO add your handling code here:
    }//GEN-LAST:event_middleNameTextMouseClicked

    private void contactTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_contactTextMouseClicked
        contactText.setEditable(true);        // TODO add your handling code here:
    }//GEN-LAST:event_contactTextMouseClicked

    private void couponCodeTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_couponCodeTextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_couponCodeTextActionPerformed

    private void jPanel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel20MouseClicked
        subtPt = custPt - Integer.parseInt(equivPoints);
        customerService.updatePoint(custID, String.valueOf(subtPt));
        customerService.addPointTransactions(custID,custID,rewID,equivPoints);
        customerPoints1.setText(String.valueOf(subtPt));
        redeemDialog.dispose();
        popUp.showMessage("Succesfully Redeemed reward!", 0);
    }//GEN-LAST:event_jPanel20MouseClicked

    private void jPanel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel21MouseClicked
        redeemDialog.dispose();
    }//GEN-LAST:event_jPanel21MouseClicked

    private void customersTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customersTableMouseReleased
        customersTable();
    }//GEN-LAST:event_customersTableMouseReleased

    private void couponTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_couponTableMouseReleased
        coupons();
    }//GEN-LAST:event_couponTableMouseReleased

    private void promotionsTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_promotionsTableMouseReleased
        promotions();
    }//GEN-LAST:event_promotionsTableMouseReleased

    private void rewardsTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardsTableMouseReleased
        rewards();
    }//GEN-LAST:event_rewardsTableMouseReleased

    private void pointsTransactTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pointsTransactTableMouseReleased
        points();
    }//GEN-LAST:event_pointsTransactTableMouseReleased

    private void addRewardsBtn2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addRewardsBtn2MouseClicked
        custPt = Double.parseDouble(customerPoints1.getText());

        if(custPt<Double.parseDouble(equivPoints))
        {
            popUp.showMessage("Insufficient Points to redeem selected Reward", 1);

        }
        else
        {
            rewardNameLabel.setText(rewName);
            redeemDialog.setVisible(true);
        }
    }//GEN-LAST:event_addRewardsBtn2MouseClicked

    private void rewardsTable3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rewardsTable3MouseReleased
        int selectedRow = rewardsTable3.getSelectedRow();
        rewID = rewardsTable3.getValueAt(selectedRow, 0).toString();
        rewName = rewardsTable3.getValueAt(selectedRow, 1).toString();
        equivPoints = rewardsTable3.getValueAt(selectedRow, 4).toString();
    }//GEN-LAST:event_rewardsTable3MouseReleased

    private void jScrollPane10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane10MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jScrollPane10MouseClicked

    private void discPercentageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_discPercentageMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_discPercentageMouseClicked

    private void discPercentageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discPercentageActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discPercentageActionPerformed

    private void discPercentage1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_discPercentage1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_discPercentage1MouseClicked

    private void discPercentage1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discPercentage1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discPercentage1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CustomerServiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CustomerServiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CustomerServiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CustomerServiceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CustomerServiceFrame(auth).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel addCoupon;
    private javax.swing.JPanel addCouponBtn;
    private javax.swing.JPanel addCustomer;
    private javax.swing.JPanel addCustomerBtn;
    private javax.swing.JLabel addProductBack3;
    private javax.swing.JLabel addProductBack4;
    private javax.swing.JLabel addProductBack5;
    private javax.swing.JLabel addProductBack6;
    private javax.swing.JLabel addProductBack7;
    private javax.swing.JLabel addProductBack8;
    private javax.swing.JPanel addPromo;
    private javax.swing.JPanel addPromoBtn;
    private javax.swing.JPanel addReward;
    private javax.swing.JPanel addRewardsBtn;
    private javax.swing.JPanel addRewardsBtn2;
    private javax.swing.JLabel addSupplierMessage;
    private javax.swing.JLabel addSupplierMessage1;
    private javax.swing.JLabel addSupplierMessage2;
    private javax.swing.JLabel addSupplierMessage3;
    private javax.swing.JTextField addressText;
    private javax.swing.JComboBox<String> brandIdCbox;
    private javax.swing.JComboBox<String> brandIdCbox1;
    private javax.swing.JComboBox<String> categoryIdCbox;
    private javax.swing.JComboBox<String> categoryIdCbox1;
    private javax.swing.JTextField contactText;
    private javax.swing.JPanel coupon;
    private javax.swing.JTextField couponCodeText;
    private org.jdesktop.swingx.JXDatePicker couponDateEnd;
    private org.jdesktop.swingx.JXDatePicker couponDateStart;
    private javax.swing.JTextField couponDescriptionText;
    private javax.swing.JLabel couponLabel;
    private javax.swing.JPanel couponSaveBtn;
    private org.jdesktop.swingx.JXTable couponTable;
    private javax.swing.JPanel couponsBtn;
    private javax.swing.JPanel custRewardBtn;
    private javax.swing.JLabel customerLabel;
    private javax.swing.JLabel customerName;
    private javax.swing.JLabel customerName1;
    private javax.swing.JLabel customerPoints;
    private javax.swing.JLabel customerPoints1;
    private javax.swing.JPanel customerSaveBtn;
    private javax.swing.JPanel customers;
    private javax.swing.JPanel customersBtn;
    private org.jdesktop.swingx.JXTable customersTable;
    private javax.swing.JPanel dashBoardBtn;
    private javax.swing.JPanel dashboard;
    private javax.swing.JLabel deleteCouponBtn;
    private javax.swing.JLabel deletePromoBtn;
    private javax.swing.JLabel deleteRewardBtn;
    private javax.swing.JTextField discPercentage;
    private javax.swing.JTextField discPercentage1;
    private javax.swing.JLabel editCouponBtn;
    private javax.swing.JPanel editPointVarButton;
    private javax.swing.JPanel editPointVariable;
    private javax.swing.JLabel editPromoBtn;
    private javax.swing.JLabel editRewardBtn;
    private javax.swing.JTextField equivalentPointsText;
    private javax.swing.JTextField firstNameText;
    private javax.swing.JComboBox<String> genderCBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel123;
    private javax.swing.JLabel jLabel124;
    private javax.swing.JLabel jLabel125;
    private javax.swing.JLabel jLabel126;
    private javax.swing.JLabel jLabel127;
    private javax.swing.JLabel jLabel128;
    private javax.swing.JLabel jLabel129;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel130;
    private javax.swing.JLabel jLabel131;
    private javax.swing.JLabel jLabel133;
    private javax.swing.JLabel jLabel134;
    private javax.swing.JLabel jLabel135;
    private javax.swing.JLabel jLabel136;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel147;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel229;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel230;
    private javax.swing.JLabel jLabel231;
    private javax.swing.JLabel jLabel232;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel240;
    private javax.swing.JLabel jLabel241;
    private javax.swing.JLabel jLabel242;
    private javax.swing.JLabel jLabel243;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel263;
    private javax.swing.JLabel jLabel264;
    private javax.swing.JLabel jLabel265;
    private javax.swing.JLabel jLabel266;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel41;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel44;
    private javax.swing.JPanel jPanel47;
    private javax.swing.JPanel jPanel48;
    private javax.swing.JPanel jPanel49;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel50;
    private javax.swing.JPanel jPanel51;
    private javax.swing.JPanel jPanel59;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel60;
    private javax.swing.JPanel jPanel65;
    private javax.swing.JPanel jPanel67;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel74;
    private javax.swing.JPanel jPanel75;
    private javax.swing.JPanel jPanel76;
    private javax.swing.JPanel jPanel77;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel83;
    private javax.swing.JPanel jPanel84;
    private javax.swing.JPanel jPanel85;
    private javax.swing.JPanel jPanel86;
    private javax.swing.JPanel jPanel87;
    private javax.swing.JPanel jPanel88;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanel90;
    private javax.swing.JPanel jPanel92;
    private javax.swing.JPanel jPanel94;
    private javax.swing.JPanel jPanel95;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator21;
    private javax.swing.JSeparator jSeparator22;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField lastNameText;
    private javax.swing.JPanel main;
    private javax.swing.JTextField middleNameText;
    private javax.swing.JLabel name;
    private org.jdesktop.swingx.JXTable newCustomersTable;
    private javax.swing.JLabel pointLabel;
    public javax.swing.JTextField pointVarValue;
    private org.jdesktop.swingx.JXTable pointsTable;
    private org.jdesktop.swingx.JXTable pointsTransactTable;
    private javax.swing.JComboBox<String> productIdCbox;
    private javax.swing.JComboBox<String> productIdCbox1;
    private javax.swing.JScrollPane productTableScroll;
    private javax.swing.JScrollPane productTableScroll1;
    private javax.swing.JScrollPane productTableScroll2;
    private javax.swing.JScrollPane productTableScroll3;
    private javax.swing.JScrollPane productTableScroll4;
    private javax.swing.JPanel profileSetting;
    private org.jdesktop.swingx.JXDatePicker promoDateEndD8;
    private org.jdesktop.swingx.JXDatePicker promoDateStartD8;
    private javax.swing.JTextField promoDescriptionText;
    private javax.swing.JTextField promoNameText;
    private javax.swing.JPanel promoSaveBtn;
    private javax.swing.JLabel promotionLabel;
    private javax.swing.JPanel promotions;
    private javax.swing.JPanel promotionsBtn;
    private org.jdesktop.swingx.JXTable promotionsTable;
    private javax.swing.JPanel prompt;
    private javax.swing.JLabel promptIcon;
    private javax.swing.JLabel promptMsg;
    private javax.swing.JPanel redeem;
    private javax.swing.JDialog redeemDialog;
    private javax.swing.JTextField rewardDescText;
    private javax.swing.JTextField rewardItem;
    private javax.swing.JLabel rewardLabel;
    private javax.swing.JLabel rewardNameLabel;
    private javax.swing.JTextField rewardNameText;
    private javax.swing.JPanel rewardSelect;
    private javax.swing.JPanel rewards;
    private javax.swing.JPanel rewardsBtn;
    private javax.swing.JPanel rewardsSaveBtn;
    private org.jdesktop.swingx.JXTable rewardsTable;
    private org.jdesktop.swingx.JXTable rewardsTable3;
    private javax.swing.JComboBox<String> searchByCust;
    private javax.swing.JComboBox<String> searchByCust2;
    private javax.swing.JComboBox<String> searchByCust3;
    private javax.swing.JComboBox<String> searchByCust4;
    private javax.swing.JComboBox<String> searchByCust5;
    private javax.swing.JComboBox<String> searchByPromo;
    private javax.swing.JTextField searchCoupon;
    private javax.swing.JTextField searchCustomer;
    private javax.swing.JTextField searchPromo;
    private javax.swing.JTextField searchReward;
    private javax.swing.JTextField searchReward1;
    private javax.swing.JComboBox<String> subCategoryIdCbox;
    private javax.swing.JComboBox<String> subCategoryIdCbox1;
    private javax.swing.JLabel title;
    private javax.swing.JPanel varSettingBtn;
    private javax.swing.JPanel viewProfile;
    // End of variables declaration//GEN-END:variables

    public void switchTo(JPanel panel,  String headerTitle){
        main.removeAll();
        main.add(panel);
        main.revalidate();
        main.repaint();
        title.setText(headerTitle);
    }
    
    private void initialize(){
        //Set this frame location to center.
        setLocationRelativeTo(null);
        popUp = new PopUpMessage(promptIcon, promptMsg, prompt, main);
        
        //switch to dashboard panel.
        
        
       // placeHolders();
        
        //paint table header background to black
        rewardsTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        couponTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        promotionsTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        customersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        newCustomersTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        pointsTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        rewardsTable3.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        pointsTransactTable.getTableHeader().setDefaultRenderer(new CustomTableHeader(new Color(102, 102, 102)));
        updateDeleteDeactivate();
        loadRewards();
        loadCoupons();
        loadPromos();
        loadCustomers();
        loadPointTransactions();
        loadNewCustomers();
        loadCustomerPoints();
        countCustomers();
        countPromotions();
        countRewards();
        countCoupons();
        calculatePoint();
        customerService.loadBrand(brandIdCbox, "-- Select Brand --",String.valueOf(categoryIdCbox.getSelectedIndex()),String.valueOf(subCategoryIdCbox.getSelectedIndex()));
        customerService.loadCategory(categoryIdCbox, "-- Select Category --");
      //  customerService.loadSubCategory(subCategoryIdCbox, "-- Select Sub Category --", );
      //  customerService.loadProduct(productIdCbox, "-- Select Product --",,,);
        
        
        
       
        
    }
    
    private void updateDeleteActivate(){
        deleteCouponBtn.setEnabled(true);
        deleteCouponBtn.setVisible(true);
        editCouponBtn.setEnabled(true);
        editCouponBtn.setVisible(true);
        deleteRewardBtn.setEnabled(true);
        deleteRewardBtn.setVisible(true);
        editRewardBtn.setEnabled(true);
        editRewardBtn.setVisible(true);
        deletePromoBtn.setEnabled(true);
        deletePromoBtn.setVisible(true);
        editPromoBtn.setEnabled(true);
        editPromoBtn.setVisible(true);
        custRewardBtn.setEnabled(true);
        custRewardBtn.setVisible(true);
    }
    private void updateDeleteDeactivate(){
        deleteCouponBtn.setEnabled(false);
        deleteCouponBtn.setVisible(false);
        editCouponBtn.setEnabled(false);
        editCouponBtn.setVisible(false);
        deleteRewardBtn.setEnabled(false);
        deleteRewardBtn.setVisible(false);
        editRewardBtn.setEnabled(false);
        editRewardBtn.setVisible(false);
        deletePromoBtn.setEnabled(false);
        deletePromoBtn.setVisible(false);
        editPromoBtn.setEnabled(false);
        editPromoBtn.setVisible(false);
        custRewardBtn.setEnabled(false);
        custRewardBtn.setVisible(false);
    }
    
    
        
     /*private void addRewards(){
        try {
            customerService.addRewards(rewName,rewItem, equivPoints);
            DatabaseConnection.ConnectDb().close();
            loadRewards();
        } catch (Exception ex) {
            Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
     
     private void addCoupons(){
        try {
             customerService.addCoupons(coupCode,desc,dateStart,dateEnd);
            DatabaseConnection.ConnectDb().close();
            loadCoupons();
        } catch (Exception ex) {
            Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
     
     
             
     
      private void updateRewards(){
        try {
            customerService.updateRewards(rewID,rewName, rewItem, equivPoints);
            DatabaseConnection.ConnectDb().close();
            loadRewards();
        } catch (Exception ex) {
            Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
      
       private void updateCoupons(){
        try {
            customerService.updateRewards(rewID,rewName, rewItem, equivPoints);
            DatabaseConnection.ConnectDb().close();
            loadCoupons();
        } catch (Exception ex) {
            Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    
    } */
    private void loadRewards(){
        try {
            rewardsTable.setModel(DbUtils.resultSetToTableModel(customerService.getRewards()));
            pointsTransactTable.setModel(DbUtils.resultSetToTableModel(customerService.getRewards()));
            ResultSet result = customerService.getRewards();
            
            while(result.next()){
            
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
            //Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadCoupons(){
        try {
            couponTable.setModel(DbUtils.resultSetToTableModel(customerService.getCoupons()));
            ResultSet result = customerService.getCoupons();
            
            while(result.next()){
                
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadPromos(){
        try {
            promotionsTable.setModel(DbUtils.resultSetToTableModel(customerService.getPromos()));
            ResultSet result = customerService.getPromos();
            
            while(result.next()){
            
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadPointTransactions(){
        try {
            pointsTransactTable.setModel(DbUtils.resultSetToTableModel(customerService.getPointTransactions(custID)));
            ResultSet result = customerService.getPromos();
            
            while(result.next()){
            
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadCustomers(){
        try {
            customersTable.setModel(DbUtils.resultSetToTableModel(customerService.getCustomers()));
            ResultSet result = customerService.getCustomers();
            
            while(result.next()){
            
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadNewCustomers(){
        try {
            newCustomersTable.setModel(DbUtils.resultSetToTableModel(customerService.getNewCustomers()));
            ResultSet result = customerService.getNewCustomers();
            
            while(result.next()){
            
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadCustomerPoints(){
        try {
            pointsTable.setModel(DbUtils.resultSetToTableModel(customerService.getCustomerPoints()));
            ResultSet result = customerService.getCustomerPoints();
            
            while(result.next()){
            
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    private void searchRewards(){
        try {
            rewardsTable.setModel(DbUtils.resultSetToTableModel(customerService.searchRewards(searchReward.getText(), selectedIndex)));
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void searchCoupons(){
        try {
            couponTable.setModel(DbUtils.resultSetToTableModel(customerService.searchCoupons(searchCoupon.getText(), selectedIndex)));
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void searchPromos(){
        try {
            promotionsTable.setModel(DbUtils.resultSetToTableModel(customerService.searchPromos(searchPromo.getText(), selectedIndex)));
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void searchCustomers(){
        try {
            customersTable.setModel(DbUtils.resultSetToTableModel(customerService.searchCustomers(searchCustomer.getText(), selectedIndex)));
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
           // Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void removeRewards(){
        try {
            customerService.removeRewards(rewID);
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
          //  Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        loadRewards();
    }
    private void removeCoupons(){
        try {
            customerService.removeCoupons(coupID);
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
          //  Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        loadCoupons();
    }
    private void removePromos(){
        try {
            customerService.removePromos(promoID);
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
         //   Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        loadCoupons();
    }
    private void updateRewards(){
        try {
            customerService.updateRewards(rewID, rewName, rewItem, rewDesc, equivPoints);
            DatabaseConnection.ConnectDb().close();
            loadRewards();
        } catch (Exception ex) {
         //   Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    private void updateCoupons(){
        try {
            customerService.updateCoupons(coupID, coupCode, promoDesc, dateStart, dateEnd);
            DatabaseConnection.ConnectDb().close();
            loadCoupons();
        } catch (Exception ex) {
         //   Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }

    public void countCustomers(){
        try {
            ResultSet result = customerService.countCustomers();
            while(result.next()){
                customerLabel.setText(String.valueOf(result.getInt("bCount")));
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
         //   Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void countPromotions(){
        try {
            ResultSet result = customerService.countPromotions();
            while(result.next()){
                promotionLabel.setText(String.valueOf(result.getInt("pCount")));
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
          //  Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void countRewards(){
        try {
            ResultSet result = customerService.countRewards();
            while(result.next()){
                rewardLabel.setText(String.valueOf(result.getInt("bCount")));
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
          //  Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void countCoupons(){
        try {
            ResultSet result = customerService.countCoupons();
            while(result.next()){
                couponLabel.setText(String.valueOf(result.getInt("pCount")));
            }
            DatabaseConnection.ConnectDb().close();
        } catch (SQLException ex) {
          //  Logger.getLogger(InventoryFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void calculatePoint(){
        
        pointVar = Double.parseDouble(customerService.getPointVariable());
        Double total = 100 * pointVar;
        pointLabel.setText(String.valueOf(total));
        
        
    }
    
     private void updatePointVar(String newVar){
        
        customerService.updatePointsVariable(newVar);
      
        
        
    }
     
    public void setName(String empName){
        name.setText(empName);
    }
    
    public void customersTable(){
        updateDeleteActivate();
        int selectedRow = customersTable.getSelectedRow();
        custID = (customersTable.getValueAt(selectedRow, 0).toString());
        lastName= customersTable.getValueAt(selectedRow, 1).toString();
        firstName = customersTable.getValueAt(selectedRow, 2).toString();
        middleName = customersTable.getValueAt(selectedRow, 3).toString();
        custPoint = (customersTable.getValueAt(selectedRow, 4).toString());
        customerPoints.setText(custPoint);
        customerPoints1.setText(custPoint);
        customerName1.setText(lastName+", "+firstName+" "+middleName);
        customerName.setText(lastName+", "+firstName+" "+middleName);
        customerService.updateCustomers(custID, lastName, firstName, middleName);// TODO add your handling code here:
        customerService.updateLog(name.getText(), title.getText(), custID, dateNow);
    }
    
    public void coupons(){
        updateDeleteActivate();
        int selectedRow = couponTable.getSelectedRow();
        coupID = (couponTable.getValueAt(selectedRow, 0).toString());
        coupCode= couponTable.getValueAt(selectedRow, 1).toString();
        coupDesc = couponTable.getValueAt(selectedRow, 2).toString();
        dateStart = couponTable.getValueAt(selectedRow, 3).toString();
        dateEnd = couponTable.getValueAt(selectedRow, 4).toString();
        //rewardNameText.setText(rewItem);
       // equivalentPointsText.setText(equivPoints);   
        customerService.updateCoupons(coupID, coupCode, coupDesc, dateStart, dateEnd);
        customerService.updateLog(name.getText(), title.getText(), coupID, dateNow);
    }
    
    public void promotions(){
        updateDeleteActivate();
        int selectedRow = promotionsTable.getSelectedRow();
        promoID = (promotionsTable.getValueAt(selectedRow, 0).toString());
        promoName= promotionsTable.getValueAt(selectedRow, 1).toString();
        promoDesc = promotionsTable.getValueAt(selectedRow, 2).toString();
        dateStart = promotionsTable.getValueAt(selectedRow, 3).toString();
        dateEnd = promotionsTable.getValueAt(selectedRow, 4).toString();
        rewardNameText.setText(rewItem);
        equivalentPointsText.setText(equivPoints);
        customerService.updatePromos(promoID, promoName, promoDesc, promoCategory, promoDateStart, promoDateEnd);
        customerService.updateLog(name.getText(),title.getText(), promoID, dateNow);
    }
    
    public void rewards(){
        updateDeleteActivate();
        int selectedRow = rewardsTable.getSelectedRow();
        rewID = (rewardsTable.getValueAt(selectedRow, 0).toString());
        rewName= rewardsTable.getValueAt(selectedRow, 1).toString();
        rewItem= rewardsTable.getValueAt(selectedRow, 2).toString();
        rewDesc= rewardsTable.getValueAt(selectedRow, 3).toString();
        equivPoints = rewardsTable.getValueAt(selectedRow, 4).toString();  
        customerService.updateRewards(rewID, rewName, rewItem, rewDesc, equivPoints);
        customerService.updateLog(name.getText(), title.getText(), rewID, dateNow);
    }
    
    public void points(){
        updateDeleteActivate();
        int selectedRow = pointsTransactTable.getSelectedRow();
        rewID = (rewardsTable.getValueAt(selectedRow, 0).toString());
        rewName= rewardsTable.getValueAt(selectedRow, 1).toString();
        rewItem= rewardsTable.getValueAt(selectedRow, 2).toString();
        rewDesc= rewardsTable.getValueAt(selectedRow, 3).toString();
        equivPoints = rewardsTable.getValueAt(selectedRow, 4).toString();  
        rewardNameText.setText(rewItem);
        equivalentPointsText.setText(equivPoints);
        customerService.updateRewards(rewID, rewName, rewItem, rewDesc, equivPoints); 
    }
}
