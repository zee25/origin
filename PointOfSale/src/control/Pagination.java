package control;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXButton;

public class Pagination implements ActionListener{
    
    public static final Color SET_COLOR = new Color(0,40,40);
    public static final Color ENABLE_COLOR = new Color(40,90,90);
    public static final Color DISABLE_COLOR = new Color(159,185,189);
    
    private int totalPageNo;
    private int offset;
    private int rowLimit;
    private int counter = 0;
    private int counter1 = 0;
    private int totalButtons;
    private String curPage;
    
    private JPanel paginationPanel;
    private ArrayList<JXButton> arrowButtons = new ArrayList<>();
    private ArrayList<JXButton> numberButtons = new ArrayList<>();
    
    public Pagination(JPanel paginationPanel, int totalButtons){
        this.paginationPanel = paginationPanel;
        this.totalButtons = totalButtons;
        refresh();
        firstPageBtn();
    }
    
    private void refresh(){
        arrowButtons.clear();
        numberButtons.clear();
        offset = 0;
        counter = 0;
        counter1 = 1;
        rowLimit = 0;
        totalPageNo = 0;
        
        if(totalButtons == 5){
            for(int i = 0; i < this.paginationPanel.getComponentCount(); i++){
                JXButton button = (JXButton) paginationPanel.getComponent(i);
                button.addActionListener(this);
                if(i >= 2 && i <= 6)
                    numberButtons.add(button);
                else
                    arrowButtons.add(button);
            }
        }
        else{
            for(int i = 0; i < this.paginationPanel.getComponentCount(); i++){
                JXButton button = (JXButton) paginationPanel.getComponent(i);
                button.addActionListener(this);
                if(i >= 2 && i <= 4)
                    numberButtons.add(button);
                else
                    arrowButtons.add(button);
            }
        }
    }
    
    public void firstPageBtn(){
        counter = 0;
        counter1 = 0;
        offset = 0;
        for(int i = 0; i < numberButtons.size(); i++)
            numberButtons.get(i).setText(String.valueOf((i + 1)));
        
        checkButtons();
        
        focusButton(numberButtons.get(0));
        
        if(numberButtons.get(0).getText().equals("1") && numberButtons.get(1).getBackground().getRGB() == DISABLE_COLOR.getRGB()){
            disableButton(arrowButtons.get(0));
            disableButton(arrowButtons.get(1));
            disableButton(arrowButtons.get(2));
            disableButton(arrowButtons.get(3));
        }
        else{
            disableButton(arrowButtons.get(0));
            disableButton(arrowButtons.get(1));
            enableButton(arrowButtons.get(2));
            enableButton(arrowButtons.get(3));
        }
        
        if (arrowButtons.get(0).isEnabled()) {
            disableButton(arrowButtons.get(0));
            disableButton(arrowButtons.get(1));
            focusButton(numberButtons.get(0));
            enableButton(arrowButtons.get(2));
            enableButton(arrowButtons.get(3));
        }
    }
    
    public void prevBtn(){
        if(arrowButtons.get(1).isEnabled()){
            counter1 = 0;
            counter--;
            offset = offset - rowLimit;
            for(int i = numberButtons.size() - 1; i >= 0; i--){
                if(numberButtons.get(i).getBackground().getRGB() == SET_COLOR.getRGB()){
                    if(counter == -1){
                        focusButton(numberButtons.get(4));
                        enableButton(numberButtons.get(0));
                        prevChangeNumberText();
                        counter = 4;
                    }
                    else{
                        focusButton(numberButtons.get(i - 1));
                        enableButton(numberButtons.get(i));
                    }
                    break;
                }
            }

            if(numberButtons.get(0).getText().equals("1") && numberButtons.get(0).getBackground().getRGB() == SET_COLOR.getRGB()){
                disableButton(arrowButtons.get(0));
                disableButton(arrowButtons.get(1));
                offset = 0;
            }
            System.out.println("Counter: "+counter);
            enableButton(arrowButtons.get(2));
            enableButton(arrowButtons.get(3));
        }
    }
    
    public void chooseBtn(JXButton button){
        if (button.isEnabled()) {
            for (int i = 0; i < numberButtons.size(); i++) {
                enableButton(numberButtons.get(i));
            }
            
            checkButtons();
            focusButton(button);
            
            int curPageNo = Integer.parseInt(button.getText());
            
            if (curPageNo == totalPageNo) {
                enableButton(arrowButtons.get(0));
                enableButton(arrowButtons.get(1));
                disableButton(arrowButtons.get(2));
                disableButton(arrowButtons.get(3));
            } else if (curPageNo == 1) {
                disableButton(arrowButtons.get(0));
                disableButton(arrowButtons.get(1));
                enableButton(arrowButtons.get(2));
                enableButton(arrowButtons.get(3));
            } else {
                enableButton(arrowButtons.get(0));
                enableButton(arrowButtons.get(1));
                enableButton(arrowButtons.get(2));
                enableButton(arrowButtons.get(3));
            }
        }
    }
    
    public void nextBtn(){
        if(arrowButtons.get(2).isEnabled()){
            counter1 = 0;
            counter++;
            offset = offset + rowLimit;
            for(int i = 0; i < numberButtons.size(); i++){
                if(numberButtons.get(i).getBackground().getRGB() == SET_COLOR.getRGB()){
                    if(counter == totalButtons){
                        focusButton(numberButtons.get(0));
                        enableButton(numberButtons.get(4));
                        nextChangeNumberText();
                        counter = 0;
                    }
                    else{
                        focusButton(numberButtons.get(i + 1));
                        enableButton(numberButtons.get(i));
                    }
                    
                    int curPageNo = 0;
                    if(counter == 0){
                        curPageNo = Integer.parseInt(numberButtons.get(0).getText());
                    }
                    else{
                        curPageNo = Integer.parseInt(numberButtons.get(i).getText()) + 1;
                    }
                    
                    if(curPageNo == (totalPageNo )){
                        disableButton(arrowButtons.get(2));
                        disableButton(arrowButtons.get(3));
                    }
                    break;
                }
            }

            enableButton(arrowButtons.get(0));
            enableButton(arrowButtons.get(1));
        }
    }
    
    public void lastPageBtn(){
        if(arrowButtons.get(3).isEnabled()){
            counter1 = 0;
            for(int i = counter; i < totalPageNo - 1; i++){
                counter++;
                if(i < totalPageNo - 1){
                    offset = offset + rowLimit;
                }
                
                if(numberButtons.get(i).getBackground().getRGB() == SET_COLOR.getRGB()){
                    if(counter == totalButtons){
                        focusButton(numberButtons.get(0));
                        enableButton(numberButtons.get(4));
                        nextChangeNumberText();
                        counter = 0;
                    }
                    else{
                        focusButton(numberButtons.get(i + 1));
                        enableButton(numberButtons.get(i));
                    }
                    
                    int curPageNo = 0;
                    if(counter == 0){
                        curPageNo = Integer.parseInt(numberButtons.get(0).getText());
                    }
                    else{
                        curPageNo = Integer.parseInt(numberButtons.get(i).getText()) + 1;
                    }
                    
                    if(curPageNo == (totalPageNo )){
                        disableButton(arrowButtons.get(2));
                        disableButton(arrowButtons.get(3));
                    }
                }
            }
//            for(int i = 0; i < totalPageNo; i++){
//                
//                if(i < totalPageNo - 1){
//                    offset = offset + rowLimit;
//                }
//                
//                for(int x = 0; x < numberButtons.size(); x++){
//                    if(counter1 == 5){
//                        focusButton(numberButtons.get(0));
//                        enableButton(numberButtons.get(4));
//                        nextChangeNumberText();
//                        counter1 = 0;
//                    }
//                    else{
//                        focusButton(numberButtons.get(x + 1));
//                        enableButton(numberButtons.get(x));
//                    }
//                    
//                    int curPageNo = 0;
//                    if(counter1 == 0){
//                        curPageNo = Integer.parseInt(numberButtons.get(0).getText());
//                    }
//                    else{
//                        curPageNo = Integer.parseInt(numberButtons.get(x).getText()) + 1;
//                    }
//                    
//                    if(curPageNo == (totalPageNo)){
//                        disableButton(arrowButtons.get(2));
//                        disableButton(arrowButtons.get(3));
//                    }
//                    break;
//                }
//                counter1++;
//            }
            enableButton(arrowButtons.get(0));
            enableButton(arrowButtons.get(1));
        }
    }
        

    @Override
    public void actionPerformed(ActionEvent e) {
        JXButton button = (JXButton) e.getSource();
        
        if(button == numberButtons.get(0)){
            counter = 0;
            counter1 = 0;
            offset = (Integer.parseInt(numberButtons.get(0).getText()) - 1) * rowLimit;
            chooseBtn(numberButtons.get(0));
            if(numberButtons.get(0).getText().equals("1")){
                disableButton(arrowButtons.get(0));
                disableButton(arrowButtons.get(1));
            }
        }
        else if(button == numberButtons.get(1)){
            counter = 1;
            counter1 = 0;
            offset = (Integer.parseInt(numberButtons.get(1).getText()) - 1) * rowLimit;
            chooseBtn(numberButtons.get(1));
        }
        else if(button == numberButtons.get(2)){
            counter = 2;
            counter1 = 0;
            offset = (Integer.parseInt(numberButtons.get(2).getText()) - 1) * rowLimit;
            chooseBtn(numberButtons.get(2));
        }
        else if(button == numberButtons.get(3)){
            counter = 3;
            counter1 = 0;
            offset = (Integer.parseInt(numberButtons.get(3).getText()) - 1) * rowLimit;
            chooseBtn(numberButtons.get(3));
        }
        else if(button == numberButtons.get(4)){
            counter = 4;
            counter1 = 0;
            offset = (Integer.parseInt(numberButtons.get(4).getText()) - 1) * rowLimit;
            chooseBtn(numberButtons.get(4));
        }
        
        switch(button.getText()){
            case "<<":
                firstPageBtn();
                break;
            case "<":
                prevBtn();
                break;
            case ">":
                nextBtn();
                break;
            case ">>":
                lastPageBtn();
                break;
        }
    }
    
    private void enableButton(JXButton button){
        button.setEnabled(true);
        button.setBackground(ENABLE_COLOR);
    }
    
    private void disableButton(JXButton button){
        button.setEnabled(false);
        button.setBackground(DISABLE_COLOR);
    }
    
    private void focusButton(JXButton button){
        curPage = button.getText();
        button.setBackground(SET_COLOR);
    }
    
    private void nextChangeNumberText(){
        numberButtons.get(0).setText(String.valueOf(Integer.parseInt(numberButtons.get(0).getText()) + totalButtons));
        numberButtons.get(1).setText(String.valueOf(Integer.parseInt(numberButtons.get(1).getText()) + totalButtons));
        numberButtons.get(2).setText(String.valueOf(Integer.parseInt(numberButtons.get(2).getText()) + totalButtons));
        numberButtons.get(3).setText(String.valueOf(Integer.parseInt(numberButtons.get(3).getText()) + totalButtons));
        numberButtons.get(4).setText(String.valueOf(Integer.parseInt(numberButtons.get(4).getText()) + totalButtons));
        
        checkButtons();
        
        focusButton(numberButtons.get(0));
    }
    
    private void prevChangeNumberText(){
        numberButtons.get(0).setText(String.valueOf(Integer.parseInt(numberButtons.get(0).getText()) - totalButtons));
        numberButtons.get(1).setText(String.valueOf(Integer.parseInt(numberButtons.get(1).getText()) - totalButtons));
        numberButtons.get(2).setText(String.valueOf(Integer.parseInt(numberButtons.get(2).getText()) - totalButtons));
        numberButtons.get(3).setText(String.valueOf(Integer.parseInt(numberButtons.get(3).getText()) - totalButtons));
        numberButtons.get(4).setText(String.valueOf(Integer.parseInt(numberButtons.get(4).getText()) - totalButtons));
        
        checkButtons();
        
        focusButton(numberButtons.get(4));
    }
    
    public void disable(){
        for(int i = 0; i < arrowButtons.size(); i++){
            disableButton(arrowButtons.get(i));
        }
        
        for(int i = 0; i < numberButtons.size(); i++){
            disableButton(numberButtons.get(i));
        }
    }
    
    private void checkButtons(){
        for (int i = 0; i < numberButtons.size(); i++) {
            int curPageNo = Integer.parseInt(numberButtons.get(i).getText());
            if (curPageNo > totalPageNo) {
                disableButton(numberButtons.get(i));
            }
            else{
                enableButton(numberButtons.get(i));
            }
        }
    }
    
    public int numberOfPage(int offset, int rowLimit){
        int totalPage = 0;
        if(offset % rowLimit == 0)
            totalPage = (int) offset / rowLimit;
        else
            totalPage = (int) offset / rowLimit + 1;
        return totalPage;
    }
    
    public void setTotalNumberOfButtons(int totalButtons){
        this.totalButtons = totalButtons;
    }
    
    public int getTotalNumberOfButtons(){
        return totalButtons;
    }
    
    public void setRowLimit(int tableHeight, int rowHeight){
        this.rowLimit = (int) (tableHeight / rowHeight);
    }
    
    public int getRowLimit(){
        return rowLimit;
    }
    
    public void setOffset(int offset){
        this.offset = offset;
    }
    
    public int getOffset(){
        return offset;
    }
    
    public void setTotalPageNo(int totalPageNo){
        this.totalPageNo = totalPageNo;
    }
    
    public int getTotalPageNo(){
        return totalPageNo;
    }
    
    public String getCurPage(){
        return curPage;
    }
}
