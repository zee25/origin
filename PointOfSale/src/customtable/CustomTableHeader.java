package customtable;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CustomTableHeader extends JLabel implements TableCellRenderer {
  
    public CustomTableHeader(Color color) {
        setFont(new Font("Roboto Light", Font.BOLD, 14));
        setOpaque(true);
        setForeground(Color.WHITE);
        setBackground(color);
        setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        setPreferredSize(new Dimension(0, 30));
    }
     
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setText(value.toString());
        return this;
    }
}
