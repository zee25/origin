package customtable;

import javax.swing.table.DefaultTableModel;

public class CustomTableModel {
    
    public static int product;
    public static int brand;
    public static int supplier;
    public static int category;
    public static int subcategory;
    public static int employee;
    
    public static final DefaultTableModel employeeTableModel = new DefaultTableModel(){
        boolean[] canEdit = new boolean[]{
            true, true, true, true, true, true
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };
    
    public static final DefaultTableModel categoryTableModel = new DefaultTableModel(){
        boolean[] canEdit = new boolean[]{
            false, true
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };
    
    public static final DefaultTableModel subCategoryTableModel = new DefaultTableModel(){
        boolean[] canEdit = new boolean[]{
            false, true
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };
    
    public static final DefaultTableModel supplierTableModel = new DefaultTableModel(){
        boolean[] canEdit = new boolean[]{
            false, true, true, true, true
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
        
    };
    
    public static final DefaultTableModel brandTableModel = new DefaultTableModel(){
        boolean[] canEdit = new boolean[]{
            false, true, false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };
    
    public static final DefaultTableModel productTableModel = new DefaultTableModel(){
        boolean[] canEdit = new boolean[]{
            true, true, true, true, true, true, true, true, true, true, true, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };
    
    public static final DefaultTableModel transactionTableModel = new DefaultTableModel();
    public static final DefaultTableModel productTransTableModel = new DefaultTableModel();
    public static final DefaultTableModel pendingTableModel = new DefaultTableModel();
    
    public static void initializeCategoryTableColumn(){
        category++;
        if (category == 1) {
            categoryTableModel.addColumn("No");
            categoryTableModel.addColumn("Category Name");
        }
    }
    
    public static void clearTableRows(DefaultTableModel tableModel){
        tableModel.getDataVector().removeAllElements();
        tableModel.fireTableDataChanged();
    }
    
    public static void initializeSubCategoryTableColumn(){
        subcategory++;
        if (subcategory == 1) {
            subCategoryTableModel.addColumn("No");
            subCategoryTableModel.addColumn("Sub-category Name");
        }
    }
    
    public static void initializeSupplierTableColumn(){
        supplier++;
        if (supplier == 1) {
            supplierTableModel.addColumn("Supplier ID");
            supplierTableModel.addColumn("Supplier Name");
            supplierTableModel.addColumn("Address");
            supplierTableModel.addColumn("Contact Number");
            supplierTableModel.addColumn("Email");
        }
    }
    
    public static void initializeBrandTableColumn(){
        brand++;
        if (brand == 1) {
            brandTableModel.addColumn("No");
            brandTableModel.addColumn("Brand Name");
            brandTableModel.addColumn("Category");
            brandTableModel.addColumn("Sub-category");
        }
    }
    
    public static void initializeProductTableColumn(){
        product++;
        if (product == 1) {
            productTableModel.addColumn("Barcode");
            productTableModel.addColumn("Product Name");
            productTableModel.addColumn("Description");
            productTableModel.addColumn("Supplier");
            productTableModel.addColumn("Category");
            productTableModel.addColumn("Sub-category");
            productTableModel.addColumn("Brand Name");
            productTableModel.addColumn("Price");
            productTableModel.addColumn("Quantity");
            productTableModel.addColumn("Purchase Date");
            productTableModel.addColumn("Expiry Date");
            productTableModel.addColumn("Date Added");
        }
    }

    public static void initializeEmployeeTableColumn() {
        employee++;
        if (employee == 1) {
            employeeTableModel.addColumn("Employee ID");
            employeeTableModel.addColumn("Last Name");
            employeeTableModel.addColumn("First Name");
            employeeTableModel.addColumn("Middle Name");
            employeeTableModel.addColumn("Job Title");
            employeeTableModel.addColumn("Gender");
        }
    }
    
    public static void initializeProductTransTableColumn(){
        productTransTableModel.addColumn("Barcode");
        productTransTableModel.addColumn("Product Name");
        productTransTableModel.addColumn("Brand Name");
        productTransTableModel.addColumn("Price");
        productTransTableModel.addColumn("Quantity");
    }
    
    public static void initializeTransactionTableColumn(){
        transactionTableModel.addColumn(" ");
        transactionTableModel.addColumn("Barcode");
        transactionTableModel.addColumn("Product Name");
        transactionTableModel.addColumn("Brand Name");
        transactionTableModel.addColumn("Price");
        transactionTableModel.addColumn("Quantity");
        transactionTableModel.addColumn("Sub Total");
    }
    
    public static void initializePendingTableColumn(){
        pendingTableModel.addColumn(" ");
        pendingTableModel.addColumn("Invoice ID");
        pendingTableModel.addColumn("Cashier ID");
        pendingTableModel.addColumn("Customer ID");
        pendingTableModel.addColumn("Invoice Amount");
    }
}
