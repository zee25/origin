package employee;

import actions.CustomerServiceTask;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import org.jdesktop.swingx.JXTable;

public class CustomerService extends Employee implements CustomerServiceTask{
    
    String pointvar;
    double pointVar;
    PreparedStatement ps;
    
    @Override
    public void pointsVariable(){  
       pointVar=Double.parseDouble(getPointVariable());  
    }
    
    
    @Override
    public double calculatePoints(double total){    
       pointsVariable(); 
       double earnedPoints=total*pointVar; 
       return earnedPoints;
    }

    /**
     * Add customers to database
     * @param lastName customer's last name
     * @param firstName customer's first name
     * @param middleName customer's middle name
     * @param gender customer's gender
     */
    public void addCustomers(String customerId, String lastName, String firstName, String middleName, String gender, String datereg, String contact, String address) {
        
        sql = "insert into customers(customer_id,last_name, first_name, middle_name, gender, date_registered, customer_contact, customer_address) "
                + "values('"+customerId+"','"+lastName+"', '"+firstName+"', '"+middleName+"', '"+gender+"','"+datereg+"','"+contact+"','"+address+"')";
        connectToDatabase2(sql);    
    }
    
    /**points
     * Search specific customer from db   
     * @param customerName a customer name
     * @return customers
     */
    public ResultSet searchCustomers(String customerName, int selectedIndex){
        
        switch (selectedIndex){
        
            case 0 :
                sql = "select customer_id as 'Customer ID',"
                    + " last_name as 'Last Name',"
                    + " first_name as 'First Name',"
                    + " middle_name as 'Middle Name'"
                    + " from customers where customer_id like '%"+customerName+"%'";
                    break;
            case 1 :
                sql = "select customer_id as 'Customer ID',"
                    + " last_name as 'Last Name',"
                    + " first_name as 'First Name',"
                    + " middle_name as 'Middle Name'"
                    + " from customers where last_name like '%"+customerName+"%'";
                    break;
            case 2 :
                sql = "select customer_id as 'Customer ID',"
                    + " last_name as 'Last Name',"
                    + " first_name as 'First Name',"
                    + " middle_name as 'Middle Name'"
                    + " from customers where middle_name like '%"+customerName+"%'";
                    break;
            case 3 :
                sql = "select customer_id as 'Customer ID',"
                    + " last_name as 'Last Name',"
                    + " first_name as 'First Name',"
                    + " middle_name as 'Middle Name'"
                    + " from customers where first_name like '%"+customerName+"%'";
                    break;
        }
        return connectToDatabase1(sql);
    }
    
    /**
     * Search specific promo from db   
     * @param promoName a reward name
     */
    public ResultSet searchPromos(String promoName,int selectedIndex){
        
        switch (selectedIndex){
        
            case 0 :
                sql = "select promo_id as 'Promotion No',"
                    + " promo_name as 'Promo Name',"
                    + " promo_description as 'Promo Description',"
                    + " promo_date_start as 'Promo Start Date',"
                    + " promo_date_end as 'Promo Expiration'"
                    + " from promotions where promo_name like '%"+promoName+"%'";
                    break;
            case 1 :
                sql = "select promo_id as 'Promotion No',"
                    + " promo_name as 'Promo Name',"
                    + " promo_description as 'Promo Description',"
                    + " promo_date_start as 'Promo Start Date',"
                    + " promo_date_end as 'Promo Expiration'"
                    + " from promotions where promo_description like '%"+promoName+"%'";
                    break;
            
        }
        return connectToDatabase1(sql);
    }

    
    /**
     * Add coupons to database
     * @param couponCode unique coupon code
     * @param couponDescription coupon description
     * @param dateStart date start validity of coupon
     * @param dateEnd date expiration of coupon
     */
    public void addCoupons(String couponCode, String couponDescription, String dateStart, String dateEnd) {
        
        sql = "insert into coupons(coupon_code, coupon_description, date_active, date_expire)"
                + " values('"+couponCode+"', '"+couponDescription+"', '"+dateStart+"', '"+dateEnd+"')";
        connectToDatabase2(sql);
    }
    
    /**
     * Add rewards to database
     * @param rewardName a reward name
     * @param rewardItem a reward item or product
     * @param equivalentPoints required points to redeem
     */
    public void addRewards(String rewardName, String rewardItem, String rewDesc, String equivalentPoints) {
        
        sql = "insert into rewards(reward_name, reward_item, reward_description, equivalent_points)"
                + " values('"+rewardName+"', '"+rewardItem+"', '"+rewDesc+"', '"+equivalentPoints+"')";
        connectToDatabase2(sql);
    }
    
    /**
     * update coupons to database
     * @param couponID coupon Identification
     * @param couponCode unique coupon code
     * @param couponDescription coupon description
     * @param dateStart date start validity of coupon
     * @param dateEnd date expiration of coupon
     */
    public void updateCoupons(String couponID, String couponCode, String couponDescription, String dateStart, String dateEnd){
        
        sql = "update coupons set coupon_code = '"+couponCode+"',"
                + "coupon_description = '"+couponDescription+"',"
                + "date_active = '"+dateStart+"',"
                + "date_expire = '"+dateEnd+"'"
                + "where coupon_id = '"+couponID+"'";
        connectToDatabase2(sql);
    }
    
    /**
     * updates rewards to database
     * @param rewardID ID to delete
     * @param rewardName a reward name
     * @param rewardItem a reward item or product
     * @param equivalentPoints required points to redeem
     */
    
    public void updateRewards(String rewardID, String rewardName, String rewardItem, String rewDesc, String equivalentPoints){
        
        sql = "update rewards set reward_name = '"+rewardName+"',"
                + "reward_item = '"+rewardItem+"',"
                + "equivalent_points = '"+equivalentPoints+"',"
                + "reward_description = '"+rewDesc+"'"
                + " where reward_id = '"+rewardID+"'";
        connectToDatabase2(sql);
    }
       
    /**
     * updates point variable value
     * @param pointVarVal new value from user
     */
     public void updatePointsVariable(String pointVarVal){
        
        sql = "update pointsvariable set PointsVar = '"+pointVarVal+"' where PointsVariableID =1 ";
        connectToDatabase2(sql);
    }
    
    
    /**
     * get rewards from database
     * @return rewards from db
     */
    public ResultSet getRewards(){
        
        sql = "select reward_id as 'Reward No',"
                + " reward_name as 'Reward Name',"
                + " reward_item as 'Reward Item',"
                + " reward_description as 'Reward Description',"
                + " equivalent_points as 'Equivalent Points' from rewards";
        return connectToDatabase1(sql);
    }
    
    
    public ResultSet getRewardsAvail(String customerPoint) {
        
        try{
            connection = ConnectDb();
            PreparedStatement ps = connection.prepareStatement(
            "select reward_id as 'Reward No',"
                + " reward_name as 'Reward Name',"
                + " reward_item as 'Reward Item',"
                + " reward_description as 'Reward Description',"
                + " equivalent_points as 'Equivalent Points' from rewards");
                result = ps.executeQuery();  
                return result;
               
                }
         catch (SQLException ex) {
            System.out.println(ex);
            return null;
        }
    }
    
    
    /**
     * get coupons from  database
     * @return coupons from db
     */
     public ResultSet getCoupons(){
        
        sql = "select coupon_id as 'Coupon No',"
                + " coupon_code as 'Coupon Code', "
                + "coupon_description as 'Coupon Description', "
                + "date_active as 'Date Active', "
                + "date_expire as 'Date Expire' from coupons";
        return connectToDatabase1(sql);    
    }
    
    /**
     * Search specific reward from db   
     * @param rewardName a reward name
     */
    public ResultSet searchRewards(String rewardName, int selectedIndex){
        
        switch (selectedIndex) {
            
            case 0 :
                sql = "select reward_id as 'Reward No',"
                    + " reward_name as 'Reward Name',"
                    + " reward_item as 'Reward Item',"
                    + " reward_description as 'Reward Description'"
                    + " equivalent_points as 'Equivalent Points'"               
                    + " from rewards where reward_id like '%"+rewardName+"%'";
                    break;
            case 1 :
                sql = "select reward_id as 'Reward No',"
                    + " reward_name as 'Reward Name',"
                    + " reward_item as 'Reward Item',"
                    + " reward_description as 'Reward Description'"
                    + " equivalent_points as 'Equivalent Points'"               
                    + " from rewards where reward_name like '%"+rewardName+"%'";
                    break;
            case 2 :
                sql = "select reward_id as 'Reward No',"
                    + " reward_name as 'Reward Name',"
                    + " reward_item as 'Reward Item',"
                    + " reward_description as 'Reward Description'"
                    + " equivalent_points as 'Equivalent Points'"               
                    + " from rewards where reward_item like '%"+rewardName+"%'";
                    break;
            case 3 :
                sql = "select reward_id as 'Reward No',"
                    + " reward_name as 'Reward Name',"
                    + " reward_item as 'Reward Item',"
                    + " reward_description as 'Reward Description'"
                    + " equivalent_points as 'Equivalent Points'"               
                    + " from rewards where reward_description like '%"+rewardName+"%'";
                    break;
            case 4 :
                sql = "select reward_id as 'Reward No',"
                    + " reward_name as 'Reward Name',"
                    + " reward_item as 'Reward Item',"
                    + " reward_description as 'Reward Description'"
                    + " equivalent_points as 'Equivalent Points'"               
                    + " from rewards where equivalent_points like '%"+rewardName+"%'";
                    break;
        }
        return connectToDatabase1(sql);
    }
    
    /**
     * Search specific coupon from db   
     * @param couponName a coupon name
     */
    public ResultSet searchCoupons(String couponName, int selectedIndex){
        
        switch (selectedIndex){
            
            case 0 :
                sql = "select coupon_id as 'Coupon No',"
                    + " coupon_code as 'Coupon Code',"
                    + " coupon_description as 'Coupon Description',"
                    + " date_active as 'Date Active',"
                    + " date_expire as 'Date Expire'"
                    + "  from coupons where coupon_code like '%"+couponName+"%'";
                    break;
            case 1 :
                sql = "select coupon_id as 'Coupon No',"
                    + " coupon_code as 'Coupon Code',"
                    + " coupon_description as 'Coupon Description',"
                    + " date_active as 'Date Active',"
                    + " date_expire as 'Date Expire'"
                    + "  from coupons where coupon_description like '%"+couponName+"%'";
                    break;
                   
                    
        }
        return connectToDatabase1(sql);
    }
    
    /**
     * remove reward from database
    * @param rewardID ID to delete
     */
    public void removeRewards(String rewardID){
        
        sql = "delete from rewards where reward_id = '"+rewardID+"'";
        connectToDatabase2(sql);
    }
    
    
    /**
     * remove coupon from database
     * @param couponID ID to delete
     */
    public void removeCoupons(String couponID){
        
        sql = "delete from coupons where coupon_id = '"+couponID+"'";
        connectToDatabase2(sql);
    }
    
    
    /**
     * Remove promo from database
     * @param promoID ID for deletion
     */
    public void removePromos(String promoID){
        
        sql = "delete from promotions where promo_id = '"+promoID+"'";
        connectToDatabase2(sql);
    }


    /**
     * Add promos to database
     * @param promoName a promotion name
     * @param promoDesc promotion description
     * @param promoCategory promotion category
     * @param promoDateStart promotion start date
     * @param promoDateEnd promotion end date
     */
    public void addPromos(String promoName,String promoDesc,String promoCategory,String promoDateStart,String promoDateEnd){
                                                                                                                            
        sql = "insert into promotions(promo_name, promo_description, promo_category,promo_date_start, promo_date_end)"
                + " values('"+promoName+"', '"+promoDesc+"', '"+promoCategory+"', '"+promoDateStart+"', '"+promoDateEnd+"')";
        connectToDatabase2(sql);
    }

    /**
     * update customers to database
     * @param custID customer's Identification
      * @param lastName customer's last name
     * @param firstName customer's first name
     * @param middleName customer's middle name
     * @param gender customer's gender
     */
    public void updateCustomers(String custID, String lastName, String firstName, String middleName){
        
        sql = "update customers set last_name = '"+lastName+"',"
                + "first_name = '"+firstName+"',"
                + "middle_name = '"+middleName+"'"
                + " where customer_id = '"+custID+"'";
        connectToDatabase2(sql);
    }

    /**
     *update promos to database
     * @param promoName a promotion name
     * @param promoDesc promotion description
     * @param promoCategory promotion category
     * @param promoDateStart promotion start date
     * @param promoDateEnd promotion end date
     */
    public void updatePromos(String promoName,String promoDesc,String promoCategory,String promoDateStart,String promoDateEnd){
       
        sql = "update promotions set promo_name = '"+promoName+"',"
                + "promo_description = '"+promoDesc+"',"
                + "promo_category = '"+promoCategory+"',"
                + " promo_date_start = '"+promoDateStart+"'"
                + " where promo_date_end = '"+promoDateEnd+"'";
        connectToDatabase2(sql);
    }

    
    @Override
    public void removeCustomer(String customerID) {
        
    }

    /**
     * get promos from database
     * @return promos from db
     */ 
    public ResultSet getPromos(){
        
        sql = "select promo_id as 'Promotion No', promo_name as 'Promo Name', "
                + "promo_description as 'Promo Description', "
                + "promo_date_start as 'Promo Start Date', "
                + "promo_date_end as 'Promo Expiration' from promotions";
        return connectToDatabase1(sql);
    }

    /**
     * get customers from database
     * @return customers from db
     */
    public ResultSet getCustomers(){
        
        sql = "select customers.customer_id as 'Customer ID', customers.last_name as 'Last Name', customers.first_name as 'First Name', customers.middle_name as 'Middle Name', points.point_amount as 'Available Points', points.overall_points as 'Overall Points' from customers INNER JOIN points ON customers.customer_id=points.customer_id";
        return connectToDatabase1(sql);
    }
    
    /**
     * get new customers from database
     * @return new customers from db
     */
    public ResultSet getNewCustomers(){
        
        sql = "select customer_id as 'Customer ID', last_name as 'Last Name', first_name as 'First Name', middle_name as 'Middle Name' from customers ORDER by date_registered DESC";
        return connectToDatabase1(sql);
    }

    /**
     * get customers from database
     * @return customers from db
     */
    public void addLog(String username, String action, String timestamp ){
        
        sql = "insert into logs(user, user_action, timestamp) values('"+username+"', '"+action+"', '"+timestamp+"')";
        connectToDatabase2(sql);
       
        
    /**
     * get customers from database
     * @return customers from db
     */    
    }
    public void updateLog(String username, String action, String idNum, String timestamp){
        
        sql = "insert into logs(user, user_action, timestamp) values('"+username+"', '"+action+"in ID no"+idNum+"', '"+timestamp+"')";
        connectToDatabase2(sql);
    
        
     /**
     * get customers from database
     * @return customers from db
     */   
    }
    public void removeLog(String username, String action, String idNum, String timestamp){
        
        sql = "insert into logs(user, user_action, timestamp) values('"+username+"', '"+action+"in ID no"+idNum+"', '"+timestamp+"')";
        connectToDatabase2(sql);
        
    }

    /**
     * get customerpoints from database
     * @return customerpoints from db
     */
    public ResultSet getCustomerPoints(){
        
        sql = "select concat(last_name ,' ', first_name,' ', middle_name) as 'Customer Name', points.point_amount as 'Point Amount', points.overall_points as 'Total Points Earned' from customers INNER JOIN points ON customers.customer_id=points.customer_id";
        return connectToDatabase1(sql);
    }

   /**
     * get customers from database
     * @return customers from db
     */
    public ResultSet countCustomers()
    {   
        sql = "select count(*) as 'bcount' from customers";
        return connectToDatabase1(sql);
    }
    
    /**
     * get customers from database
     * @return customers from db
     */
    public ResultSet countPromotions()
    {    
        sql = "select count(*) as 'pcount' from promotions";
        return connectToDatabase1(sql);
    }
    
    /**
     * get customers from database
     * @return customers from db
     */
    public ResultSet countRewards()
    {   
        sql = "select count(*) as 'bcount' from rewards";
        return connectToDatabase1(sql);
    }
    
    /**
     * get customers from database
     * @return customers from db
     */
    public ResultSet countCoupons()
    {    
        sql = "select count(*) as 'pcount' from coupons";
        return connectToDatabase1(sql);
    }
    
    /**
     * retrieves point variable value   
     */
    public String getPointVariable()
    {        
        try{        
            sql = "select PointsVar from pointsvariable where PointsVariableID=1";
            connection = ConnectDb();
            statement = connection.prepareStatement(sql);
            result = statement.executeQuery();
            while(result.next()){
                //Retrieve by column name
            pointvar  = result.getString("PointsVar");   
            }
            result.close();
            return pointvar;
            
        } 
        catch (SQLException ex) {
            return pointvar;
        }       
    }

    @Override
    public void addPoints(double pointEarned)
    {
        
    }

    @Override
    public void regPoints(String custId)
    {
        
        sql = "insert into points(customer_id, point_amount, overall_points)"
                + " values('"+custId+"', '1', '1')";
        connectToDatabase2(sql);
    }

    @Override
    public ResultSet specificPoint(String custID)
    {    
        sql = "select count(*) as 'pcount' from coupons";
        return connectToDatabase1(sql);
    }
    
    @Override
    public ResultSet getLastCustId(){
        sql= "select(customer_id) FROM customers";
        return connectToDatabase1(sql);
        
    }
    
    @Override
    public ResultSet getPointTransactions(String custID)
    {
        sql = "select rewards_transaction.transaction_id as 'Transaction ID', rewards.reward_name as 'Reward Name',rewards.equivalent_points as'Points Spent' from rewards_transaction "
                + "JOIN customers ON rewards_transaction.customer_id=customers.customer_id "
                + "JOIN points on rewards_transaction.point_id=points.point_id "
                + "JOIN rewards on rewards_transaction.reward_id=rewards.reward_id where rewards_transaction.customer_id='"+custID+"' ";
        return connectToDatabase1(sql); 
        
    }
    
    @Override
    public int getTotalCustomers() {
        try {
            int total = 0;
            sql = "select count(customer_id) as 'value' from customers";
            result = connectToDatabase1(sql);
            if(result.next())
                total = result.getInt("value");
            
            return total;
        } catch (SQLException ex) {
            Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public void addPointTransactions(String custId, String pointId, String rewardId, String equivalentPoints) {
        sql = "insert into rewards_transaction(customer_id, point_id, reward_id, equivalent_points)"
                + " values('"+custId+"','"+pointId+"', '"+rewardId+"','"+equivalentPoints+"')";
        connectToDatabase2(sql);
    }

    

    @Override
    public void updatePromos(String promoID, String promoName, String promoDescription, String promoCategory, String promoDateStart, String promoDateEnd) {
        sql = "update promotions set promo_name = '"+promoName+"',"
                + "promo_description = '"+promoDescription+"',"
                + "promo_category = '"+promoCategory+"',"
                + " promo_date_start = '"+promoDateStart+"'"
                + " where promo_date_end = '"+promoDateEnd+"'";
        connectToDatabase2(sql);
    }
    
    @Override
    public void updatePoint(String pointId, String subtPoint){
         sql = "update points set point_amount= '"+subtPoint+"'"
                + " where customer_id = '"+pointId+"'";
        connectToDatabase2(sql);
    }
    
    @Override
    public void loadBrand(JComboBox comboBox, String firstIndex, String selCat, String selSub) {
        sql = "select brand_name from brand";
        result = connectToDatabase1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        try{
            while(result.next()){
                comboBox.addItem(result.getString("brand_name"));
        }
        connection.close();
        }
        catch(Exception E){
            
        }
    }
    
    @Override
    public void loadCategory(JComboBox comboBox, String firstIndex) {
        sql = "select category_name from category";
        result = connectToDatabase1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        try
        {
            while(result.next()){
                comboBox.addItem(result.getString("category_name"));
        }
        connection.close();
        }
        catch(Exception E){
               
                }
    }
    
    @Override
    public void loadSubCategory(JComboBox comboBox, String firstIndex, String selCat) {
        sql = "select sub_category_name from sub_category";
        result = connectToDatabase1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        try{
            while(result.next()){
                comboBox.addItem(result.getString("sub_category_name"));
        }
            connection.close();
        }
        catch(Exception E)
        {
           
        }
    }
    
    @Override
    public void loadProduct(JComboBox comboBox, String firstIndex, String selCat, String selSub, String brand) {
        sql = "select product_name from product";
        result = connectToDatabase1(sql);
        comboBox.removeAllItems();
        comboBox.addItem(firstIndex);
        try{
        while(result.next()){
            comboBox.addItem(result.getString("product_name"));
        }
        connection.close();
        }catch(Exception E)
        {
            
        }
    }

    @Override
    public void addCustomer(String customerId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException {
        sql = "insert into customers(customer_id, last_name, first_name, middle_name, gender.gender, customer_address, customer_contact) values('"+customerId+"', '"+lastName+"', '"+firstName+"', '"+midName+"', '"+gender+"', '"+address+"', '"+contNo+"')";
        connect2(sql);
    }

    @Override
    public void loadCustomer(JXTable table, String fieldName, String order, int offset, int rowLimit) throws SQLException {
        sql = "select customer_id as 'Customer ID', ";
    }

    @Override
    public void updateCustomer(String curCustId, String newCustId, String lastName, String firstName, String midName, int gender, String address, String contNo) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerId(String curCustId, String newCustId) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerLastName(String custId, String lastName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerFirstName(String custId, String firstName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerMiddleName(String custId, String midName) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerGender(String custId, int gender) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerAddress(String custId, String address) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCustomerContactNumber(String custId, String contactNo) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
